<?php


Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {
    Route::get('system-setting', 'SystemSettingController@getAll');
    Route::post('update-tx', 'WebhookController@onReceiveTransaction');

    Route::resource('users', 'UsersController')->only('store');
    Route::post('users/reset-password', 'UsersController@resetPassword')->name('users.resetPassword');
    Route::post('users/submit-reset-password', 'UsersController@submitResetPassword')->name('users.submitResetPassword');

    Route::resource('whitelists', 'WhitelistsController')->only('store');

    Route::post('userKycs/callback', 'UserKycsController@getApplicantCallback')->name('userKycs.getApplicantCallback');

    Route::post('/send-verify-user', 'UsersController@sendVerifyUser');

    Route::group(['middleware' => ['auth:api', 'singleSession']], function () {
        Route::group(['prefix' => 'conversions', 'as' => 'conversions.'], function () {
            Route::get('/', 'ConversionController@getUserConversions')->name('conversions');
            Route::post('/', 'ConversionController@store')->name('convert');
            Route::get('settings', 'ConversionController@getConversionSettings');
            Route::get('/sold', 'ConversionController@getSoldTokensCount')->name('getSoldTokensCount');
        });

        Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
            Route::get('current-user', 'UsersController@getCurrentUser')->name('getCurrentUser');
            Route::put('update-user-info-after-kyc', 'UsersController@updateUserInfoAfterKyc')->name('updateUserInfoAfterKyc');
            Route::get('referral-program-records', 'UsersController@getReferralProgramRecords');
            Route::get('est-heta-tokens', 'UsersController@getEstHetaTokens');
            Route::get('total-heta-tokens', 'UsersController@getTotalHetaTokens');
            Route::get('qr-code-google-url', 'UsersController@getQRCodeGoogleUrl')->name('getQRCodeGoogleUrl');
            Route::post('add-security-setting-otp', 'UsersController@addSecuritySettingOtp')->name('addSecuritySettingOtp');
            Route::post('remove-security-setting-otp', 'UsersController@removeAuthentication')->name('removeAuthentication');
            Route::post('change-address', 'UsersController@changeAddress')->name('changeAddress');
        });

        Route::resource('users', 'UsersController')->except('store', 'index', 'show', 'update');

        Route::group(['prefix' => 'userKycs', 'as' => 'userKycs.'], function () {
            Route::get('get-sumsub-token', 'UserKycsController@getSumSubToken')->name('getSumSubToken');
            Route::get('get-applicant', 'UserKycsController@getApplicant')->name('getApplicant');
        });

        Route::group(['prefix' => 'wallet', 'as' => 'wallet.'], function () {
            Route::get('transactions', 'WalletController@getTransactions')->name('getTransactions');
            Route::get('/', 'WalletController@getAll');
            Route::post('generate-deposit-address', 'WalletController@generateDepositAddress');
            Route::get('heta-fluctuations', 'WalletController@getHetaFluctuations');
        });

        Route::resource('userKycs', 'UserKycsController')->only('store');

        Route::group(['prefix' => 'bountySettings', 'as' => 'bountySettings.'], function () {
            Route::get('/', 'BountySettingsController@getAll')->name('getAll');
        });

        Route::group(['prefix' => 'bounties', 'as' => 'bounties.'], function () {
            Route::get('social', 'BountiesController@getSocialBounty')->name('getSocialBounty');
            Route::get('posts', 'BountiesController@getAllPosts')->name('getAllPosts');
            Route::get('register', 'BountiesController@getBountyRegister')->name('getBountyRegister');
            Route::get('vouchers', 'BountiesController@getAllBountyVoucher')->name('getAllBountyVoucher');
            Route::post('vouchers', 'BountiesController@createBountyVoucher')->name('createBountyVoucher');
        });

        Route::resource('bounties', 'BountiesController')->only('store');
        Route::resource('bugBounties', 'BugBountiesController')->only('store');
        Route::get('/bugBounties/get-all', 'BugBountiesController@getAllBugBounties')->name('bugBounties.getAllBugBounties');

        Route::get('prices', 'PriceController@getPrices');
        Route::get('bonuses', 'BonusesController@getAll')->name('getAll');
    });
});
