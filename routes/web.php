<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/email/verify/{token}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/admin-login', 'Auth\LoginController@adminLogin')->name('adminLogin');
Route::put('/locale', 'Api\UsersController@setLocale');

Route::group(['middleware' => ['singleSession', 'bindSession', 'createFreshApiToken']], function () {
    Route::get('/password/reset/{token}', 'HomeController@resetPassword')->name('resetPassword');
    Route::get('login', 'HomeController@index');
    Route::get('register', 'HomeController@index');
    Route::get('reset-password', 'HomeController@index');
    Route::get('whitelist/create', 'HomeController@index');
    Route::get('terms', 'HomeController@index');
    Route::get('guide', 'HomeController@index');
    Route::get('faq', 'HomeController@index');
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('login', 'HomeController@admin')->name('admin.login');

    Route::group(['middleware' => ['auth:admin', 'isAdmin']], function () {
        Route::get('/', 'HomeController@admin');
        Route::post('logout', 'Auth\LoginController@adminLogout')->name('adminLogout');
        Route::get('users', 'HomeController@admin');
        Route::get('whitelists', 'HomeController@admin');
        Route::get('users/report', 'HomeController@admin');
        Route::get('bounties', 'HomeController@admin');
        Route::get('bug-bounties', 'HomeController@admin');
        Route::get('bounty-settings', 'HomeController@admin');
        Route::get('vouchers', 'HomeController@admin');
        Route::get('settings', 'HomeController@admin');
        Route::get('cold-wallets', 'HomeController@admin');
        Route::get('statistic', 'HomeController@admin');
        Route::get('distribution', 'HomeController@admin');
//        Route::get('export/distribution-hetatokens', 'ExportController@exportDistributionHetaTokens');
    });
});

Route::group(['prefix' => 'api', 'namespace' => 'Api', 'middleware' => ['auth:admin', 'isAdmin', 'build-api']], function () {
    Route::get('/users', 'UsersController@getAll')->name('users.getAll');
    Route::get('/whitelists', 'WhitelistsController@getAll')->name('whitelists.getAll');
    Route::get('/admin/users/get-user-report', 'UsersController@getUserReport')->name('users.getUserReport');
    Route::get('/stats/spent-vs-deposited', 'WalletController@getSpentVsDepositedStats');
    Route::get('cold-wallets', 'ColdWalletsController@getAll');
    Route::put('/bountySettings/{bountySetting}', 'BountySettingsController@update');
    Route::get('/bounties', 'BountiesController@getAll')->name('bounties.getAll');
    Route::put('/bounties/{bounty}', 'BountiesController@updateStatus')->name('bounties.update_status');
    Route::post('/bounties/approve-all-type', 'BountiesController@approveAllType');
    Route::get('/bugBounties', 'BugBountiesController@getAll')->name('bugBounties.getAll');
    Route::post('/bugBounties/approve-all', 'BugBountiesController@approveAll')->name('bugBounties.approveAll');
    Route::put('/bugBounties/{bounty}', 'BugBountiesController@updateStatus')->name('bugBounties.updateStatus');
    Route::get('/vouchers', 'VouchersController@getAll')->name('vouchers.getAll');
    Route::resource('vouchers', 'VouchersController')->only('store', 'update');
    Route::get('/wallet/distribution-heta-tokens', 'WalletController@getDistributionHetaTokens');
    Route::put('/wallet/approve-bounty-withdraw', 'WalletController@approveBountyWithdraw');
    Route::resource('bonuses', 'BonusesController')->only('store', 'update');

    Route::put('system-setting', 'SystemSettingController@update');
    Route::get('conversion-statistic', 'ConversionController@getConversionStatistics');
    Route::get('kyc-statistic', 'UsersController@getKYCStatistics');
    Route::group(['prefix' => '/admin/bountySettings', 'as' => 'bountySettings.'], function () {
        Route::get('/', 'BountySettingsController@getAll')->name('getAll');
    });
    Route::get('/admin/bonuses', 'BonusesController@getAll')->name('getAll');
});

Route::group(['middleware' => ['auth', 'singleSession', 'bindSession', 'createFreshApiToken']], function () {
    Route::get('/', 'HomeController@index');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::put('change-password', 'Api\UsersController@changePassword')->name('changePassword');

    Route::get('dashboard', 'HomeController@index');
    Route::get('bounty', 'HomeController@index');
    Route::get('users/kyc', 'HomeController@index');
    Route::get('users/profileSetting', 'HomeController@index');
    Route::get('balances', 'HomeController@index');

    Route::get('deposit', 'HomeController@index');
    Route::get('wallet/transactions', 'HomeController@index');
    Route::get('token-history', 'HomeController@index');
    Route::get('users/profileSetting/change-password', 'HomeController@index');

    Route::get('/export/wallet/transactions', 'ExportController@exportWalletTransactions');
    Route::get('deposit/history', 'HomeController@index');
    Route::get('faq', 'HomeController@index');
    Route::get('deposit', 'HomeController@index');
    Route::get('/users/profileSetting/2FA', 'HomeController@index');
});
