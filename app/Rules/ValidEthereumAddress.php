<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Murich\PhpCryptocurrencyAddressValidation\Validation\ETH as ETHValidator;

class ValidEthereumAddress implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validator = new ETHValidator($value);
        return $validator->validate();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be a valid ethereum address.';
    }
}
