<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ColumnListFetchable;

class Whitelist extends Model
{
    use ColumnListFetchable;

    protected $fillable = [
        'email',
        'name',
        'eth_address',
        'eth_amount',
    ];
}
