<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ColumnListFetchable;

class AddressHistory extends Model
{
    use ColumnListFetchable;

    protected $fillable = [
        'user_id',
        'address',
        'currency',
    ];
}
