<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use App\Models\Traits\ColumnListFetchable;
use App\Notifications\ResetPassword;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, ColumnListFetchable;

    const ADMIN_ROLE = 'admin';
    const USER_ROLE = 'user';
    const KYC_STATUS_PENDING = 1;
    const KYC_STATUS_VERIFIED = 2;
    const KYC_STATUS_REJECTED = 3;

    protected $fillable = [
        'name',
        'email',
        'password',
        'kyc_status',
        'referral_id',
        'referrer_id',
        'eth_address',
        'btc_address',
        'usdt_address',
        'token_address',
        'two_factor_secret',
        'two_factor_enabled',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'is_deposited',
        'referrees',
        'kycs',
        'deposit',
    ];

    public function sendPasswordResetNotification($token)
    {
        $url = route('resetPassword', ['token' => $token]);
        $this->notify(new ResetPassword($this->email, "{$url}?email={$this->email}"));
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->referral_id = $user->randomRefferalId();
        });
    }

    public function getIsDepositedAttribute()
    {
        return !!$this->transactions()->where('type', Transaction::TYPE_DEPOSIT)->where('status', Transaction::STATUS_SUCCEEDED)->count();
    }

    public function getReferreesAttribute()
    {
        return $this->bounties()->referralRegistered()->count();
    }

    public function getKycsAttribute()
    {
        return $this->bounties()->referralKyced()->count();
    }

    public function getDepositAttribute()
    {
        return $this->bounties()->referralDeposited()->count();
    }

    public function isAdmin()
    {
        return $this->role === self::ADMIN_ROLE;
    }

    public function wallets() {
        return $this->hasMany(Wallet::class);
    }

    public function hetaFluctuations() {
        return $this->hasMany(HetaFluctuation::class)->orderBy('id', 'desc');
    }

    public function verificationUrl()
    {
        return route('verification.verify', $this->verifyUser->token);
    }

    public function bounties()
    {
        return $this->hasMany(Bounty::class);
    }

    public function bugBounties()
    {
        return $this->hasMany(BugBounty::class);
    }

    public function referrer()
    {
        return $this->belongsTo(self::class, 'referrer_id');
    }

    public function refer()
    {
        return $this->hasMany(self::class, 'referrer_id');
    }

    public function verifyUser()
    {
        return $this->hasOne('App\Models\VerifyUser');
    }

    public function userKyc()
    {
        return $this->hasOne('App\Models\UserKyc');
    }

    public function addressHistories()
    {
        return $this->hasMany('App\Models\AddressHistory')->orderBy('id', 'desc');
    }

    public function transactions()
    {
        return $this->hasMany('App\Models\Transaction');
    }

    public function randomRefferalId()
    {
         $id = str_random(8);

         $validator = \Validator::make(['referral_id' => $id], ['referral_id' => 'unique:users,referral_id']);

         if($validator->fails()){
              return $this->randomRefferalId();
         }

         return $id;
    }
}
