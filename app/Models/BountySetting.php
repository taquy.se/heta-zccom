<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ColumnListFetchable;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class BountySetting extends Model
{
    use Cachable;
    use ColumnListFetchable;

    protected $fillable = [
        'key',
        'value',
        'display_order',
        'display_name',
    ];
}
