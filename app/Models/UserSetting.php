<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class UserSetting extends Model
{
	use Cachable;

    protected $fillable = [
        'user_id',
        'key',
        'value'
    ];
}
