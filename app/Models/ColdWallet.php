<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ColdWallet extends Model
{
    use Cachable;

    protected $fillable = [
        'currency',
        'address',
        'updated_at',
    ];
}
