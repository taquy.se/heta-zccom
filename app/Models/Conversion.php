<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversion extends Model
{
    public $timestamps = false;
    public $table = 'conversions';

    public $fillable = [
        'user_id',
        'email',
        'currency_to_usd_price',
        'token_to_currency_price',
        'currency_amount',
        'currency',
        'token_amount',
        'bonus_percent',
        'bonus_amount',
        'total_token_amount',
        'created_day_london',
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
