<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ColumnListFetchable;

class Voucher extends Model
{
    use ColumnListFetchable;

    protected $fillable = [
        'code',
        'expires_at',
        'token',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($voucher) {
            $voucher->code = $voucher->randomCodeUnique();
        });
    }

    public function randomCodeUnique()
    {
        $code = str_random(10);

        $validator = \Validator::make(['code' => $code], ['code' => 'unique:vouchers,code']);

        if($validator->fails()){
             return $this->randomCodeUnique();
        }

        return $code;
    }
}
