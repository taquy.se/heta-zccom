<?php

namespace App\Models;

use App\Models\Traits\ColumnListFetchable;
use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    use ColumnListFetchable;

    protected $fillable = [
        'timestamp',
        'percent'
    ];
}
