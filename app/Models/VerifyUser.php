<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VerifyUser extends Model
{
    protected $fillable = [
        'user_id',
        'token',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($verifyUser) {
            $verifyUser->token = $verifyUser->randomTokenUnique();
        });
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function randomTokenUnique()
    {
        $token = str_random(30);

         $validator = \Validator::make(['token' => $token], ['token' => 'unique:verify_users,token']);

         if($validator->fails()){
              return $this->randomTokenUnique();
         }

         return $token;
    }
}
