<?php

namespace App\Models\Traits;

use Illuminate\Support\Facades\Schema;

trait ColumnListFetchable
{
    public function getColumnList()
    {
        return Schema::getColumnListing($this->getTable());
    }
}
