<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class Price extends Model
{
	use Cachable;

    const BTC = 'BTC';
    const ETH = 'ETH';
    const USDT = 'USDT';

    protected $fillable = [
        'coin',
        'price',
    ];
}
