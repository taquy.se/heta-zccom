<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $timestamps = false;

    const TYPE_DEPOSIT = 'deposit';
    const TYPE_WITHDRAW = 'withdraw';

    const STATUS_WAITING = 'waiting';
    const STATUS_CONFIRMING = 'confirming';
    const STATUS_SUCCEEDED = 'succeeded';
    const STATUS_FAILED = 'failed';

    public $table = 'transactions';

    public $fillable = [
        'user_id',
        'currency',
        'amount',
        'txid',
        'to_address',
        'confirmations',
        'type',
        'status',
        'email',
        'is_confirm',
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getDateFormat()
    {
        return 'Uv';
    }
}
