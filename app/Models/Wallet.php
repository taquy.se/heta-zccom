<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    public $table = 'wallets';

    public $fillable = [
        'user_id',
        'currency',
        'balance',
        'available_balance',
        'address',
        'created_at',
        'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
