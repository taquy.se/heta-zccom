<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ConversionSetting extends Model
{
    use Cachable;

    public $table = 'conversion_settings';

    public $fillable = [
        'currency',
        'min_token',
        'currency_precision',
        'min_currency',
        'created_at',
        'updated_at',
    ];
}
