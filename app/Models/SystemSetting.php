<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class SystemSetting extends Model
{
    use Cachable;

    protected $fillable = [
        'key',
        'value',
    ];
}
