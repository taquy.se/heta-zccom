<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ColumnListFetchable;
use App\Utils;

class BugBounty extends Model
{
    use ColumnListFetchable;

    public $timestamps = false;

    const BUG_REPORT_TYPE = 'bug_report';
    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'status',
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($bounty) {
            $bounty->heta_tokens = $bounty->bountySetting()->value ?? 0;
            $bounty->created_at = Utils::currentMilliseconds();
            $bounty->updated_at = Utils::currentMilliseconds();
        });

        static::updating(function ($bounty) {
            $bounty->updated_at = Utils::currentMilliseconds();
        });
    }

    public function bountySetting()
    {
        return BountySetting::where('key', self::BUG_REPORT_TYPE)->first();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeApproved($query)
    {
        return $query->where('status', self::STATUS_APPROVED);
    }
}
