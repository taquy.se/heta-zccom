<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ColumnListFetchable;
use App\Utils;

class Bounty extends Model
{
    use ColumnListFetchable;

    const REGISTER_TYPE = 'register';
    const KYC_TYPE = 'kyc';
    const DEPOSIT_TYPE = 'deposit';
    const VOUCHER_TYPE = 'voucher';
    const REFERRAL_REGISTER_TYPE = 'referral_register';
    const REFERRAL_DEPOSIT_TYPE = 'referral_deposit';
    const REFERRAL_KYC_TYPE = 'referral_kyc';

    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'type',
        'meta_data',
        'status',
        'created_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($bounty) {
            $bounty->heta_tokens = $bounty->bountySetting->value ?? 0;
            $bounty->created_at = Utils::currentMilliseconds();

            if ($bounty->type === self::VOUCHER_TYPE) {
                $bounty->heta_tokens = $bounty->voucher->token ?? 0;
            }
        });
    }

    public function bountySetting()
    {
        return $this->belongsTo(BountySetting::class, 'type', 'key');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeReferralRegistered($query)
    {
        return $query->where('status', Bounty::STATUS_APPROVED)->where('type', Bounty::REFERRAL_REGISTER_TYPE);
    }

    public function scopeReferralKyced($query)
    {
        return $query->where('status', Bounty::STATUS_APPROVED)->where('type', Bounty::REFERRAL_KYC_TYPE);
    }

    public function scopeReferralDeposited($query)
    {
        return $query->where('status', Bounty::STATUS_APPROVED)->where('type', Bounty::REFERRAL_DEPOSIT_TYPE);
    }

    public function scopeReferralRecords($query)
    {
        return $query->where('type', 'like', 'referral%');
    }

    public function scopeApproved($query)
    {
        return $query->where('status', Bounty::STATUS_APPROVED);
    }

    public function voucher()
    {
        return $this->belongsTo(Voucher::class, 'meta_data', 'code');
    }
}
