<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HetaFluctuation extends Model
{
    public $timestamps = false;

    const CONVERSION_ACTION_TYPE = 'convert';
    const BOUNTY_ACTION_TYPE = 'bounty';

    protected $fillable = [
        'user_id',
        'ending_balance',
        'amount',
        'action_id',
        'action_type',
        'object',
        'object_type',
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'is_bounty',
    ];

    public function getIsBountyAttribute()
    {
        return $this->action_type === self::BOUNTY_ACTION_TYPE;
    }
}
