<?php

namespace App;

class Consts
{
    const DEFAULT_PER_PAGE = 10;
    const DISPLAY_ORDER_REFERRAL_PROGRAM = 1;
    const DISPLAY_ORDER_REGISTRATION = 2;
    const DISPLAY_ORDER_SNS_LIKE_SHARE = 3;
    const DISPLAY_ORDER_WRITING_FILMING_ABOUT_HETA = 4;
    const DISPLAY_ORDER_VOUCHER = 5;
    const DISPLAY_ORDER_BUG_BOUNTY = 6;

    const SOCKET_CHANNEL_USER = 'App.User.';
    const HETA_TOKEN = 'heta';

    const TRANSACTION_TYPE_DEPOSIT = 'deposit';
    const TRANSACTION_TYPE_WITHDRAW = 'withdraw';

    const CONFIRMATION_BTC = 'confirmation_btc';
    const CONFIRMATION_ETH = 'confirmation_eth';
    const CONFIRMATION_USDT = 'confirmation_usdt';

    const MIN_DEPOSIT_BTC = 'min_deposit_btc';
    const MIN_DEPOSIT_ETH = 'min_deposit_eth';
    const MIN_DEPOSIT_USDT = 'min_deposit_usdt';

    const SUPPORTED_LOCALES = ['en', 'vi', 'jp', 'fr'];

    const ICO_START_TIME = "ico_start_time";
    const ICO_END_TIME = "ico_end_time";
    const ICO_PRICE_HETA_TOKEN_PER_USD = "ico_price_heta_token_per_usd";
    const ICO_HARDCAP_BY_USD = "ico_hardcap_by_usd";

    const DEFAULT_EMAIL_INTERVAL = 0.07;
}
