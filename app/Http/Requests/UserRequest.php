<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|string|email|max:255|verify_register_email|verify_pending_email|is_not_temp_email',
            'password' => 'required|string|min:6|max:128|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/|confirmed',
            'eth_address' => 'valid_ethereum_address',
        ];
        if ($this->referral_id) {
             $rules = array_merge($rules, [
                'referral_id' => [
                    Rule::exists('users')->where(function ($query) {
                        $query->where('referral_id', $this->referral_id)
                            ->where('is_verified', true);
                    })
                ]
            ]);
        }
        return $rules;
    }
}
