<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|string|min:6|max:128|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/|no_space|different:current_password|confirmed',
            'current_password' => "required|old_password:{$this->user()->password}",
        ];
    }

    public function attributes()
    {
        return [
            'current_password' => 'current password'
        ];
    }
}
