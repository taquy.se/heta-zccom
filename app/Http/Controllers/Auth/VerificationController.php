<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Services\UserService;
use App\Jobs\BountyApproved;
use App\Models\VerifyUser;
use App\Models\Bounty;
use App\Models\User;

class VerificationController extends Controller
{
    public function verify($token)
    {
        $verify = VerifyUser::where('token', $token)->first();
        $status = false;
        $message = 'Token invalid';
        if ($verify) {
            $user = $verify->user;
            if($user && !$user->is_verified) {
                $user->is_verified = true;
                $user->save();
                $message = '';
                $status = true;

                $this->createUserWallets($user);

                $userBounty = $user->bounties()->create([
                    'type' => Bounty::REGISTER_TYPE,
                    'status' => Bounty::STATUS_APPROVED,
                ]);
                BountyApproved::dispatch($userBounty);

                $referrer = $user->referrer;
                if ($referrer) {
                    $referrerBounty = $referrer->bounties()->create([
                        'type' => Bounty::REFERRAL_REGISTER_TYPE,
                        'meta_data' => $user->email,
                        'status' => Bounty::STATUS_APPROVED,
                    ]);
                    BountyApproved::dispatch($referrerBounty);
                }
            }
        }

        return view('auth.verify', [
            'status' => $status,
            'message' => $message,
        ]);
    }

    private function createUserWallets(User $user)
    {
        return $user->wallets()->createMany([
            ['currency' => 'btc'],
            ['currency' => 'eth'],
            ['currency' => 'usdt'],
            ['currency' => 'heta'],
        ]);
    }
}
