<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserSessionUpdatedEvent;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Laravel\Passport\Token;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/';
    protected $sessionId = '';
    protected $isAdmin = false;

    public function adminLogin(Request $request)
    {
        $this->isAdmin = true;
        $this->sessionId = session('__session_id', session()->getId());

        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->guard()->validate($this->adminCredentials($request))) {
            $this->validateAuthenticationCode($request);
        }

        if ($this->attemptAdminLogin($request)) {
            return $this->sendLoginResponse($request, true);
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);
    }

    public function login(Request $request)
    {
        $this->sessionId = session('__session_id', session()->getId());

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->guard()->validate($this->credentials($request))) {
            $this->validateAuthenticationCode($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string|email_verify',
            'password' => 'required|string',
        ]);
    }

    private function validateAuthenticationCode(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user && $user->two_factor_enabled) {
            $this->validate($request, [
                'code' => "required|is_invalid_otp_code:{$user->id}",
            ]);
        }
    }

    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        $credentials['role'] = User::USER_ROLE;

        return $credentials;
    }

    private function adminCredentials(Request $request)
    {
        $credentials = $this->credentials($request);
        $credentials['role'] = User::ADMIN_ROLE;

        return $credentials;
    }

    private function attemptAdminLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->adminCredentials($request), $request->filled('remember')
        );
    }

    public function adminLogout(Request $request)
    {
        $this->isAdmin = true;
        $this->guard()->logout();
        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/admin');
    }

    protected function guard()
    {
        return $this->isAdmin ? Auth::guard('admin') : Auth::guard();
    }

    protected function sendLoginResponse(Request $request)
    {
        $this->clearUserSessionId($request);

        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        $this->storeUserSessionId($request);

        return $this->authenticated($request, $this->guard()->user()) ?: $this->redirect();
    }

    private function redirect()
    {
        if ($this->isAdmin && strpos(session('url.intended', ''), '/admin') === false) {
            session(['url.intended' => '/admin']);
        }
        if (!$this->isAdmin && strpos(session('url.intended', ''), '/admin') !== false) {
            session(['url.intended' => $this->redirectPath()]);
        }
        return redirect()->intended($this->isAdmin ? '/admin' : $this->redirectPath());
    }

    private function clearUserSessionId(Request $request)
    {
        $user = $this->guard()->user();

        $previousSessionId = $user->session_id;

        $user->session_id = null;
        $user->save();

        if ($previousSessionId && $user->role === User::USER_ROLE) {
            Event::dispatch(new UserSessionUpdatedEvent($user->id));
        }
    }

    private function storeUserSessionId(Request $request)
    {
        $user = $this->guard()->user();
        $normalUser = Auth::guard('web')->user();

        if ($user->role === User::USER_ROLE || !$normalUser) {
            $this->sessionId = session()->getId();
        }

        session(['__session_id' => $this->sessionId]);

        if (!$user->session_id && $user->role === User::USER_ROLE) {
            $user->session_id = $this->sessionId;
            $user->save();
            Token::where('user_id', $user->id)->update(['revoked' => true]);
        }
    }
}