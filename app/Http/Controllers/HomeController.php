<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Http\Services\UserService;

class HomeController extends Controller
{
    public function index()
    {
        return view('layouts.app');
    }

    public function resetPassword(Request $request, $token = null)
    {
        $userService = new UserService();
        $request->merge(['token' => $token]);
        if ($userService->isValidResetPasswordLink($request)) {
            return view('layouts.app');
        }
        return view('auth.reset_password_error');
    }

    public function admin()
    {
        return view('admin.app');
    }
}
