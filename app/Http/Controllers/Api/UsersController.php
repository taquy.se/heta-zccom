<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\SendVerifyUserRequest;
use App\Models\User;
use App\Models\VerifyUser;
use App\Notifications\VerifyEmail;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Services\UserService;
use App\Http\Services\AuthenticationService;
use Illuminate\Support\Facades\DB;
use Exception;

class UsersController extends ApiController
{
    protected $model = User::class;
    private $userService;
    private $authenticationService;

    function __construct(UserService $userService, AuthenticationService $authenticationService)
    {
        $this->userService = $userService;
        $this->authenticationService = $authenticationService;
    }

    public function store (UserRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = $this->userService->store($request);
            DB::commit();
            return $user;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function getCurrentUser (Request $request)
    {
        return $this->userService->getCurrentUser($request);
    }

    public function changeAddress (Request $request)
    {
        $this->validateAddress($request);
        $this->authenticationService->validateAuthenticationCode($request);

        DB::beginTransaction();
        try {
            $result = $this->userService->changeAddress($request);

            DB::commit();
            return $result;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function changePassword (ChangePasswordRequest $request)
    {
        $this->authenticationService->validateAuthenticationCode($request);
        DB::beginTransaction();
        try {
            $this->userService->changePassword($request);

            DB::commit();
            return 'success';
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function updateUserInfoAfterKyc (Request $request)
    {
        return $this->userService->updateUserInfoAfterKyc($request->user());
    }

    public function resetPassword (ResetPasswordRequest $request)
    {
        $this->userService->resetPassword($request);
        return 'success';
    }

    public function submitResetPassword (Request $request)
    {
        $request->validate(['password' => 'required|confirmed|min:6']);
        $this->userService->submitResetPassword($request);
        return 'success';
    }

    public function getUserReport (Request $request)
    {
        return $this->userService->getUserReport($request);
    }

    private function validateAddress (Request $request)
    {
        $this->validate($request, ['currency' => 'required|in:btc,eth,usdt,token', 'address' => 'required']);

        $currencies = ['btc' => 'bitcoin', 'eth' => 'ethereum', 'usdt' => 'tetherus', 'token' => 'ethereum'];
        $currency = $currencies[$request->currency];

        $this->validate($request,
            ['address' => "required|valid_{$currency}_address|unique_currency_address:{$request->currency},{$request->user()->id}"]
        );
    }

    public function getReferralProgramRecords(Request $request)
    {
        return $this->userService->getReferralProgramRecords($request);
    }

    public function getEstHetaTokens ()
    {
        return $this->userService->getEstHetaTokens();
    }

    public function getTotalHetaTokens (Request $request)
    {
        return $this->userService->getTotalHetaTokens($request);
    }

    public function getQRCodeGoogleUrl (Request $request)
    {
        $user = $request->user();

        if ($user->two_factor_secret) {
            $secret = $user->two_factor_secret;
        } else {
            $secret = $this->authenticationService->createSecret();
            $user->update(['two_factor_secret' => $secret]);
        }

        $qrCodeUrl = $this->authenticationService->getQRCodeGoogleUrl($user->email, $secret, 'HETACHAIN');

        return [
            'key' => $secret,
            'url' => $qrCodeUrl,
        ];
    }

    public function addSecuritySettingOtp (Request $request)
    {
        $user = $request->user();
        $this->validate($request, [
            'password' => "required|old_password:{$user->password}",
            'code' => "required|is_invalid_otp_code:{$user->id}",
        ]);

        $user->update(['two_factor_enabled' => true]);
        return $user;
    }

    public function removeAuthentication(Request $request)
    {
        $user = $request->user();
        $this->validate($request, [
            'password' => "required|old_password:{$user->password}",
            'code' => "required|is_invalid_otp_code:{$user->id}",
        ]);

        $user->update(['two_factor_enabled' => false]);
        return $user;
    }

    public function setLocale(Request $request)
    {
        Utils::setLocale($request);
        return 'ok';
    }

    public function getKYCStatistics()
    {
        return $this->userService->getKYCStatistics();
    }

    public function sendVerifyUser(SendVerifyUserRequest $request)
    {
        $user = User::where('email', $request->input('email'))->first();
        VerifyUser::create([
            'user_id' => $user->id,
        ]);

        $user->notify(new VerifyEmail($user));
        return 'ok';
    }
}
