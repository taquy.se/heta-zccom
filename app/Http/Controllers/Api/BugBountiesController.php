<?php

namespace App\Http\Controllers\Api;

use App\Consts;
use App\Http\Requests\CreateBugBountyRequest;
use Illuminate\Http\Request;
use App\Models\BugBounty;
use App\Jobs\BugBountyApproved;
use DB;
use Log;
use Exception;

class BugBountiesController extends ApiController
{
    protected $model = BugBounty::class;

    public function store(CreateBugBountyRequest $request)
    {
        return BugBounty::create($request->all());
    }

    public function getAllBugBounties(Request $request)
    {
        $request->merge(['user_id' => $request->user()->id]);
        return $this->getAll($request);
    }

    public function updateStatus(Request $request, BugBounty $bounty)
    {
        $bounty->status = $request->status;
        $bounty->save();

        if ($bounty->status === BugBounty::STATUS_APPROVED) {
            BugBountyApproved::dispatch($bounty);
        }
        return $bounty;
    }

    protected function getQueryAll(Request $request)
    {
        $model = $this->model;
        $columns = $this->getColumns($request);
        $query = $columns ? $model::select($columns) : $model::select();

        $this->addEqualFilters($query, $request);

        if ($request->has(self::LIKE_KEY)) {
            $conditions = json_decode($request->get(self::LIKE_KEY));

            //addLikeFilters
            $query->join('users', 'users.id', 'bug_bounties.user_id')->select('bug_bounties.*');
            $query->where(function ($query) use ($request, $conditions) {
                foreach ($conditions as $attribute => $value) {
                    $query->orWhere($attribute, 'LIKE', "%{$value}%");
                }
            });
        }

        //addOrderBy
        $sortField = $request->sortField;
        $sortDirection = $request->sortDirection;

        if ($sortField && $sortDirection) {
            $query->orderBy("bug_bounties.{$sortField}", $sortDirection);
        }

        return $query;
    }


    public function approveAll(Request $request)
    {
        DB::beginTransaction();
        try {
            BugBounty::where('status', BugBounty::STATUS_PENDING)
                ->chunk(100, function ($bounties) {
                    foreach ($bounties as $bounty) {
                        $bounty->status = BugBounty::STATUS_APPROVED;
                        $bounty->save();
                        BugBountyApproved::dispatch($bounty);
                    }
                });

            DB::commit();
            return 'ok';
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());

            throw $e;
        }
    }
}
