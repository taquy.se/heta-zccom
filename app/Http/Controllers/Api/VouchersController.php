<?php

namespace App\Http\Controllers\Api;

use App\Models\Voucher;
use Illuminate\Http\Request;

class VouchersController extends ApiController
{
    protected $model = Voucher::class;

    public function store(Request $request)
    {
        return Voucher::create($request->all());
    }

    public function update(Voucher $voucher, Request $request)
    {
        $voucher->update($request->all());
        return 'success';
    }
}
