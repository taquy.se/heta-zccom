<?php

namespace App\Http\Controllers\Api;

use App\Http\Services\TransactionService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebhookController extends Controller
{
    private $transactionService;

    function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    public function onReceiveTransaction(Request $request)
    {
        $coin = $request->input('coin');
        $transactionId = $request->input('txid');

        logger()->info("Detect {$coin} transaction {$transactionId}");

        $this->transactionService->transactionUpdated($coin, $transactionId);

        return response('succeeded', 200);
    }
}
