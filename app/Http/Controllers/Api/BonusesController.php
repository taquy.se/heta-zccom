<?php

namespace App\Http\Controllers\Api;

use App\Models\Bonus;
use Illuminate\Http\Request;

class BonusesController extends ApiController
{
    protected $model = Bonus::class;

    public function store(Request $request)
    {
        return Bonus::create($request->all());
    }

    public function update(Bonus $bonus, Request $request)
    {
        $bonus->update($request->all());
        return 'success';
    }
}
