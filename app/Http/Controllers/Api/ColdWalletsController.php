<?php

namespace App\Http\Controllers\Api;

use App\Models\ColdWallet;
use Illuminate\Http\Request;

class ColdWalletsController extends ApiController
{
    protected $model = ColdWallet::class;

    public function getAll(Request $request)
    {
        $coldWallets = [];

        $coldWallets[] = ColdWallet::where('currency', 'btc')->latest()->first();
        $coldWallets[] = ColdWallet::where('currency', 'eth')->latest()->first();
        $coldWallets[] = ColdWallet::where('currency', 'usdt')->latest()->first();

        return array_filter($coldWallets);
    }
}
