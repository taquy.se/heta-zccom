<?php

namespace App\Http\Controllers\Api;

use App\Models\Whitelist;
use Illuminate\Http\Request;
use App\Http\Requests\WhitelistRequest;

class WhitelistsController extends ApiController
{
    protected $model = Whitelist::class;

    public function store(WhitelistRequest $request)
    {
        return  Whitelist::create($request->all());
    }
}
