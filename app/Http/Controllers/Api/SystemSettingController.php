<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SystemSettingController extends ApiController
{
    public function getAll(Request $request)
    {
        return system_setting();
    }

    public function update(Request $request)
    {
        system_setting([$request->input('key') => $request->input('value')]);
        Artisan::call('cache:clear');

        return system_setting();
    }
}
