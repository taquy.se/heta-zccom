<?php

namespace App\Http\Controllers\Api;

use App\Consts;
use App\Http\Requests\CreateBountyRequest;
use App\Http\Services\UserService;
use App\Jobs\BountyApproved;
use App\Models\Bounty;
use App\Models\BountySetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BountiesController extends ApiController
{
    protected $model = Bounty::class;

    public function store(CreateBountyRequest $request)
    {
        return Bounty::create($request->all());
    }

    public function updateStatus(Request $request, Bounty $bounty)
    {
        $bounty->status = $request->input('status');
        $bounty->save();

        if ($bounty->status === Bounty::STATUS_APPROVED) {
            BountyApproved::dispatch($bounty);
        }
        return $bounty;
    }

    public function getSocialBounty (Request $request)
    {
        $userId = $request->user()->id;

        return Bounty::where('user_id', $userId)
            ->whereIn('type', ['facebook_share', 'google_share', 'twitter_share', 'linkedin_share', 'facebook_like', 'medium_follow', 'bitcoin_talk_follow', 'steemit_follow', 'twitter_follow', 'linkedin_connection', 'telegram_join', 'telegram_news_join', 'youtube_subscribe'])
            ->get();
    }

    public function getAllPosts (Request $request)
    {
        $userId = $request->user()->id;

        return Bounty::where('user_id', $userId)
            ->whereIn('type', ['bitcoin_talk_post', 'steemit_post', 'reddit_post', 'medium_post','linkedin_post', 'facebook_post', 'blog_post', 'other_website_post', 'other_video_post'])
            ->get();
    }

    public function getBountyRegister (Request $request)
    {
        $userId = $request->user()->id;

        return Bounty::where('user_id', $userId)
            ->whereIn('type', ['register', 'kyc', 'deposit'])
            ->get();
    }

    protected function getQueryAll(Request $request)
    {
        $model = $this->model;
        $columns = $this->getColumns($request);
        $query = $columns ? $model::select($columns) : $model::select();

        $this->addEqualFilters($query, $request);

        $conditions = json_decode($request->get(self::LIKE_KEY));

        //addLikeFilters
        $query->join('users', 'users.id', 'bounties.user_id')->select('bounties.*');
        $query->where(function ($query) use ($request, $conditions) {
            foreach ($conditions as $attribute => $value) {
                $query->orWhere($attribute, 'LIKE', "%{$value}%");
            }
        });

        //addOrderBy
        $sortField = $request->sortField;
        $sortDirection = $request->sortDirection;

        if ($sortField && $sortDirection) {
            $query->orderBy("bounties." . $sortField, $sortDirection);
        }

        $displayOrder = $request->input('display_order', '');
        if (intval($displayOrder) === Consts::DISPLAY_ORDER_VOUCHER) {
            $query->where('type', 'voucher');
        } else {
            $keys = BountySetting::where('display_order', $displayOrder)->pluck('key');
            if (count($keys)) {
                $query->whereIn('type', $keys);
            }
        }

        return $query;
    }

    public function getAllBountyVoucher (Request $request)
    {
        $userId = $request->user()->id;

        return Bounty::where('user_id', $userId)
            ->where('type', Bounty::VOUCHER_TYPE)
            ->with('voucher')
            ->get();
    }

    public function createBountyVoucher (Request $request)
    {
        $this->validateVoucher($request);

        return Bounty::create([
            'user_id' => $request->user_id,
            'type' => $request->type,
            'meta_data' => $request->voucher,
        ]);
    }

    private function validateVoucher(Request $request)
    {
        $this->validate($request, ['voucher' => "voucher_existed|unique_voucher:{$request->user_id}|voucher_is_expired"]);
    }

    public function approveAllType(Request $request, UserService $userService)
    {
        DB::beginTransaction();
        try {
            $type = $request->input('type');

            Bounty::when($type === Consts::DISPLAY_ORDER_VOUCHER, function ($query) {
                return $query->where('type', 'voucher');
            }, function ($query) use ($type) {
                $keys = BountySetting::where('display_order', $type)->pluck('key');
                $query->whereIn('type', $keys);
            })->where('status', Bounty::STATUS_PENDING)
                ->chunk(100, function ($bounties) use ($userService) {
                    foreach ($bounties as $bounty) {
                        $bounty->status = Bounty::STATUS_APPROVED;
                        $bounty->save();

                        BountyApproved::dispatch($bounty);
                    }
                });

            DB::commit();
            return 'ok';
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());

            throw $e;
        }
    }
}
