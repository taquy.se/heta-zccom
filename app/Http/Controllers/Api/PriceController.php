<?php

namespace App\Http\Controllers\Api;

use App\Http\Services\PriceService;
use Illuminate\Http\Request;

class PriceController extends ApiController
{
    private $priceService;

    /**
     * PriceController constructor.
     * @param PriceService $priceService
     */
    public function __construct(PriceService $priceService)
    {
        $this->priceService = $priceService;
    }

    public function getPrices()
    {
        return $this->priceService->getPrices();
    }
}
