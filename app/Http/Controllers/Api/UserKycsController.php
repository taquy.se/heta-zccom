<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use App\Models\UserKyc;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Bounty;
use Exception;
use App\Events\UserUpdatedEvent;

class UserKycsController extends ApiController
{
    protected $model = UserKyc::class;

    const SUMSUB_KYC_STATUS_VERIFIED = 'GREEN';

    public function store (Request $request)
    {
        return  UserKyc::create($request->all());
    }

    public function getSumSubToken (Request $request)
    {
        $client = new Client();
        $host = env('SUM_SUB_HOST');
        $key = env('SUM_SUB_API_KEY');
        $now = Carbon::now()->timestamp;
        $externalUserId = "{$now}_{$request->user()->id}";

        $url = "{$host}/resources/accessTokens?key={$key}&userId={$externalUserId}&ttlInSecs=1800";

        $response = $client->post($url);
        $data = json_decode($response->getBody()->getContents());
        return $data->token;
    }

    public function getApplicant (Request $request)
    {
        return $request->user()->userKyc;
    }

    public function getApplicantCallback (Request $request)
    {
        DB::beginTransaction();
        try {
            $digest = hash_hmac('sha1', $request->getContent(), env('SUM_SUB_API_SECRET'));

            if ($request->has('digest') && $request->digest === $digest ) {
                $applicant = UserKyc::where('applicant_id', $request->applicantId)->firstOrFail();

                $user = $applicant->user;

                if ($request->review['reviewAnswer'] === self::SUMSUB_KYC_STATUS_VERIFIED) {
                    $kycStatus = User::KYC_STATUS_VERIFIED;

                    $user->bounties()->create([
                        'type' => Bounty::KYC_TYPE,
                        'status' => Bounty::STATUS_APPROVED,
                    ]);

                    $referrer = $user->referrer;
                    if ($referrer) {
                        $referrer->bounties()->create([
                            'type' => Bounty::REFERRAL_KYC_TYPE,
                            'meta_data' => $user->email,
                        ]);
                    }
                } else {
                    $kycStatus = User::KYC_STATUS_REJECTED;
                }
                $user->update(['kyc_status' => $kycStatus]);
                event(new UserUpdatedEvent($user));
            }

            DB::commit();
            return 'success';
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
