<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

abstract class ApiController extends Controller
{
    const COLUMNS_KEY = 'columns';
    const LIKE_KEY = 'like';
    const LIKE_OPERATOR = 'like_operator';
    const DELIMITER = ',';

    public function getAll (Request $request)
    {
        if ($request->has('limit')) {
            return $this->paginate($request);
        } else {
            return $this->getAllRecords($request);
        }
    }

    protected static function getColumns(Request $request): array
    {
        if ($request->has(self::COLUMNS_KEY)) {
            return array_filter(explode(self::DELIMITER, $request->get(self::COLUMNS_KEY)));
        }
        return [];
    }

    protected function getQueryAll(Request $request)
    {
        $model = $this->model;
        $columns = $this->getColumns($request);
        $query = $columns ? $model::select($columns) : $model::select();

        $this->addEqualFilters($query, $request);
        $this->addLikeFilters($query, $request);
        $this->addOrderBy($query, $request);

        return $query;
    }

    protected function addOrderBy(Builder $query, Request $request)
    {
        $sortField = $request->sortField;
        $sortDirection = $request->sortDirection;

        if ($sortField && $sortDirection) {
            $query->orderBy($sortField, $sortDirection);
        }
    }

    protected function getAllRecords(Request $request)
    {
        $query = $this->getQueryAll($request);
        return $query->get();
    }

    protected function paginate(Request $request)
    {
        $query = $this->getQueryAll($request);
        return $query->paginate($request->limit);
    }

    protected function addLikeFilters(Builder $target, Request $request)
    {
        if ($request->get(self::LIKE_KEY, false)) {
            $params = collect(json_decode($request->get(self::LIKE_KEY)));
            $conditions = $params->only((new $this->model)->getColumnList())->all();

            $target->where(function ($query) use ($request, $conditions) {
                if ($request->get(self::LIKE_OPERATOR, false) && $request->get(self::LIKE_OPERATOR) === 'or') {
                    foreach ($conditions as $attribute => $value) {
                        $query->orWhere($attribute, 'LIKE', "%{$value}%");
                    }
                } else {

                    foreach ($conditions as $attribute => $value) {
                        $query->where($attribute, 'LIKE', "%{$value}%");
                    }
                }
            });
        }
    }

    protected function addEqualFilters(Builder $target, Request $request)
    {
        $conditions = $request->all((new $this->model)->getColumnList());
        foreach ($conditions as $attribute => $value) {
            if ($value !== null) {
                $target->whereIn($attribute, explode(self::DELIMITER, $value));
            }
        }
    }
}
