<?php

namespace App\Http\Controllers\Api;

use App\Models\BountySetting;
use Illuminate\Http\Request;

class BountySettingsController extends ApiController
{
    protected $model = BountySetting::class;

    public function getSocialBountySetting()
    {
        return BountySetting::whereIn('key', ['facebook_share', 'google_share', 'twitter_share', 'linkedin_share', 'facebook_like', 'medium_follow', 'bitcoin_talk_follow', 'steemit_follow', 'twitter_follow', 'linkedin_connection', 'telegram_join', 'telegram_news_join', 'youtube_subscribe'])
            ->get();
    }

    public function update(Request $request, BountySetting $bountySetting)
    {
        $bountySetting->value = $request->input('value', 0);
        $bountySetting->save();

        return $bountySetting;
    }
}
