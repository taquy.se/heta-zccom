<?php

namespace App\Http\Controllers\Api;

use Exception;
use App\Models\Bounty;
use App\Http\Services\WalletService;
use Illuminate\Http\Request;

class WalletController extends ApiController
{
    protected $walletService;

    function __construct(WalletService $walletService)
    {
        $this->walletService = $walletService;
    }

    public function getTransactions(Request $request) {
        return $this->walletService->getTransactions($request->all());
    }

    public function generateDepositAddress(Request $request)
    {
        $user = $request->user();
        $this->validate($request, ['coin' => 'required|in:btc,eth,usdt']);
        try {
            return $this->walletService->generateDepositAddress($user, $request->coin);
        } catch (Exception $e) {
            logger()->error($e->getMessage());
            abort(500);
        }
    }

    public function getHetaFluctuations(Request $request)
    {
        return $request->user()->hetaFluctuations()->orderBy('id', 'desc')->get();
    }

    public function getAll(Request $request)
    {
        return $request->user()->wallets()->orderBy('id')->get();
    }

    public function getSpentVsDepositedStats(Request $request)
    {
        return $this->walletService->getSpentVsDepositedStats($request->all());
    }

    public function getDistributionHetaTokens(Request $request)
    {
        return $this->walletService->getDistributionHetaTokens($request->all());
    }

    public function approveBountyWithdraw(Request $request)
    {
        return $this->walletService->approveBountyWithdraw($request->all());
    }
}
