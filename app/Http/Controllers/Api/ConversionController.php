<?php

namespace App\Http\Controllers\Api;

use App\Http\Services\ConversionService;
use Illuminate\Http\Request;
use App\Http\Requests\ConvertRequest;
use DB;
use Exception;
use Log;

class ConversionController extends ApiController
{
    private $conversionService;

    /**
     * PriceController constructor.
     * @param ConversionService $conversionService
     */
    public function __construct(ConversionService $conversionService)
    {
        $this->conversionService = $conversionService;
    }

    public function store(ConvertRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = $request->user();
            $input = $request->all();
            $input['user_id'] = $user->id;
            $input['email'] = $user->email;

            $order = $this->conversionService->create($input);
            DB::commit();
            return $order;
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            throw $e;
        }
    }


    public function getUserConversions(Request $request)
    {
        return $this->conversionService->getUserConversions($request->user()->id);
    }

    public function getConversionStatistics(Request $request)
    {
        return $this->conversionService->getConversionStatistics($request->all());
    }

    public function getSoldTokensCount(Request $request) {
        return $this->conversionService->getSoldTokenCount();
    }
    
    public function getConversionSettings(Request $request)
    {
        return $this->conversionService->conversionSetting();
    }
}
