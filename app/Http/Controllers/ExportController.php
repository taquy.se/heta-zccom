<?php

namespace App\Http\Controllers;

use App\Exports\DistributionHetaTokensExport;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Services\WalletService;
use App\Models\Transaction;
use App\Exports\TransactionsExport;
use Excel;

class ExportController extends Controller
{
    protected $walletService;

    function __construct(WalletService $walletService)
    {
        $this->walletService = $walletService;
    }

    public function exportWalletTransactions(Request $request)
    {
        $rows = [
                    [__('Status'), __('Coin'), __('Amount'), __('Date'), __('Address'), __('Txid')] //first row is title
                ];

        $fileName = 'funding_transactions.csv';
        if ($request->has('type')) {
            if ($request->type === TRANSACTION::TYPE_DEPOSIT) {
                $fileName = 'deposit_history.csv';
            } else {
                $fileName = 'withdrawal_history.csv';
            }
        }
        return Excel::download(new TransactionsExport($this->walletService, $request->all()), $fileName);
    }

    public function exportDistributionHetaTokens(Request $request)
    {
        $fileName = 'distribution_heta_tokens.csv';
        return Excel::download(new DistributionHetaTokensExport($this->walletService, $request->all()), $fileName);
    }
}
