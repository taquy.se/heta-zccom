<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\RelationNotFoundException;

class PreloadEloquentResponse
{
    /**
     - Handle an incoming request.
     *
     - @param  \Illuminate\Http\Request  $request
     - @param  \Closure  $next
     - @param  string|null  $guard
     - @return mixed
     */

    public function handle($request, Closure $next, $guard = null)
    {
        $response = $next($request);
        $original = $response->getOriginalContent();

        if (
            $original instanceof Collection ||
            $original instanceof Model ||
            $original instanceof LengthAwarePaginator
        ) {
            $preloads = explode(',', $request->get('with',''));
            try {
                $original->load($preloads);
            } catch (RelationNotFoundException $e) {
                // logger($e->getMessage());
            }
        }
        return $response;
    }
}
