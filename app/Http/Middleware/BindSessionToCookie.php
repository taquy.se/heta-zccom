<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Contracts\Session\Session;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Laravel\Passport\Passport;
use Symfony\Component\HttpFoundation\Cookie;

class BindSessionToCookie
{
    private $encrypter;
    private $session;
    public static $cookie = 'passport_session_id';

    public function __construct(Encrypter $encrypter, Session $session)
    {
        $this->encrypter = $encrypter;
        $this->session   = $session;
    }

    public static function cookie($cookie = null)
    {
        if (is_null($cookie)) {
            return static::$cookie;
        }

        static::$cookie = $cookie;

        return new $cookie;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $response = $next($request);

        if ($this->shouldReceiveFreshToken($response)) {
            $response->withCookie($this->makeCookie(
                $request->user($guard)
            ));
        }

        return $response;
    }

    protected function shouldReceiveFreshToken($response)
    {
        foreach ($response->headers->getCookies() as $cookie) {
            if ($cookie->getName() === Passport::cookie()) {
                return true;
            }
        }

        return false;
    }

    public function makeCookie(User $user)
    {
        $config = Config::get('session');

        $expiration = Carbon::now()->addMinutes($config['lifetime']);

        $token = JWT::encode([
            'sub'       => $user->getKey(),
            'sessionId' => session('__session_id', $this->session->getId()),
            'expiry'    => $expiration->getTimestamp(),
        ], $this->encrypter->getKey());

        return new Cookie(
            static::cookie(),
            $token,
            $expiration,
            $config['path'],
            $config['domain'],
            $config['secure'],
            true
        );
    }
}
