<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Exception;
use Firebase\JWT\JWT;
use Illuminate\Contracts\Encryption\Encrypter;
use Laravel\Passport\Passport;
use Illuminate\Auth\AuthenticationException;

class SingleSession
{
    private $encrypter;

    public function __construct(Encrypter $encrypter)
    {
        $this->encrypter = $encrypter;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        try {
            if ($this->isValidRequest($request)) {
                return $next($request);
            }
        } catch (Exception $e) {}

        if (!$this->isPassport() && Auth::check()) {
            Auth::logout();
        }

        throw new AuthenticationException;
    }

    public function isValidRequest($request)
    {
        if ($this->isPassport()) {
            return $this->isValidToken($request);
        }
        return $this->isValidSession($request);
    }

    public function isValidSession($request)
    {
        $sessionId = session('__session_id', session()->getId());

        $userSessionId = $request->user() ? $request->user()->session_id : null;

        return !$request->user() || ($request->user()->session_id === $sessionId);
    }

    public function isValidToken($request)
    {
        $passportCookie = $request->cookie(Passport::cookie());

        $user = Auth::guard('api')->user();
        $userSessionId = $user ? $user->session_id : null;

        return (!$passportCookie || $userSessionId === $this->decodeJwtTokenCookie($request)['sessionId']);
    }

    public function decodeJwtTokenCookie($request)
    {
        $cookie = $request->cookie(BindSessionToCookie::cookie());

        $data = (array) JWT::decode(
            $this->encrypter->decrypt($cookie, false),
            $this->encrypter->getKey(), ['HS256']
        );

        return $data;
    }

    public function isPassport()
    {
        $config = config('auth.guards');
        $currentGuard = Auth::getDefaultDriver();
        return $config[$currentGuard]['driver'] === 'passport';
    }
}
