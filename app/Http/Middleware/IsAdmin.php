<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Access\AuthorizationException;
use Closure;
use App\Models\User;

class IsAdmin
{
    public function handle($request, Closure $next)
    {
        if ($request->user()->role === User::ADMIN_ROLE) {
            return $next($request);
        }
        throw new AuthorizationException('Unauthorized');
    }
}
