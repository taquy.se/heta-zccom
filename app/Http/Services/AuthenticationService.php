<?php

namespace App\Http\Services;

use PHPGangsta_GoogleAuthenticator;
use Illuminate\Http\Request;
use App\Models\User;

class AuthenticationService
{
    private $googleAuthenticator;

    function __construct(PHPGangsta_GoogleAuthenticator $googleAuthenticator)
    {
        $this->googleAuthenticator = $googleAuthenticator;
    }

    public function validateAuthenticationCode (Request $request)
    {
        $user = User::find($request->user()->id);
        if ($user->two_factor_enabled) {
            $request->validate(['code' => "required|is_invalid_otp_code:{$user->id}"]);
        }
    }

    public function createSecret()
    {
        return $this->googleAuthenticator->createSecret();
    }

    public function getQRCodeGoogleUrl($email, $secret, $appName)
    {
        return $this->googleAuthenticator->getQRCodeGoogleUrl($email, $secret, $appName);
    }
}
