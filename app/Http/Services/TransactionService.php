<?php

namespace App\Http\Services;

use DB;
use Exception;
use App\Utils;
use App\Consts;
use App\BigNumber;
use App\Models\Bounty;
use App\Models\Wallet;
use App\Models\User;
use App\Models\Transaction;
use App\Jobs\BountyApproved;
use GuzzleHttp\Exception\ClientException;
use App\Events\TransactionUpdated as TransactionUpdatedEvent;

class TransactionService
{
    const TRANSACTION_NOT_FOUND_CODE = 404;

    private $walletService;
    private $blockchainClient;

    function __construct(BlockchainClient $blockchainClient, WalletService $walletService)
    {
        $this->walletService = $walletService;
        $this->blockchainClient = $blockchainClient;
    }

    public function transactionUpdated($coin, $transactionId)
    {
        try {
            $blockchainTx = $this->blockchainClient->getBlockchainTransaction($coin, $transactionId);
            return $this->updateBlockchainTransaction($blockchainTx);
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() === self::TRANSACTION_NOT_FOUND_CODE) {
                return $this->dropBlockchainTransaction($coin, $transactionId);
            }
            throw $e;
        }
    }

    private function updateBlockchainTransaction($blockchainTx)
    {
        DB::beginTransaction();
        try {
            if ($this->isNewDepositions($blockchainTx)) {
                $transactions = $this->createTransactions($blockchainTx);
            } else {
                $transactions = $this->updateTransactions($blockchainTx);
            }

            foreach ($transactions as $transaction) {
                event(new TransactionUpdatedEvent($transaction->user_id, $transaction));
                if ($transaction->is_confirm) {
                    $this->walletService->updateUserWalletBalance($transaction->user_id, $transaction->currency, $transaction->amount);
                    $this->createUserBounties($transaction);
                }
            }
            DB::commit();
            return $transactions;
        } catch (Exception $e) {
            DB::rollback();
            logger()->error($e->getMessage());
            throw $e;
        }
    }

    private function createUserBounties($transaction)
    {
        if ($this->isFistDeposition($transaction)) {
            $userBounty = $transaction->user->bounties()->create([
                'type' => Bounty::DEPOSIT_TYPE,
                'meta_data' => $transaction->id,
                'status' => Bounty::STATUS_APPROVED,
            ]);

            BountyApproved::dispatch($userBounty);

            $referrer = $transaction->user->referrer;
            if ($referrer) {
                $referrerBounty = $referrer->bounties()->create([
                    'type' => Bounty::REFERRAL_DEPOSIT_TYPE,
                    'meta_data' => $transaction->user->email,
                    'status' => Bounty::STATUS_APPROVED,
                ]);
                BountyApproved::dispatch($referrerBounty);
            }
        }
    }

    private function isFistDeposition($transaction)
    {
        return !Bounty::where('user_id', $transaction->user_id)
            ->where('type', Bounty::DEPOSIT_TYPE)
            ->exists();
    }

    private function isNewDepositions($blockchainTx)
    {
        return !Transaction::where('txid', $blockchainTx->txid)->where('currency', $blockchainTx->coin)->count();
    }

    private function createTransactions($blockchainTx)
    {
        $transactions = [];
        foreach ($blockchainTx->entries as $entry) {
            $wallet = $this->getWallet($blockchainTx->coin, $entry->address);
            $minAmount = $this->getMinDepositAmount($blockchainTx->coin);
            $actualAmount = $this->getActualValue($blockchainTx->coin, $entry->valueString);

            if ($wallet && (BigNumber::new($actualAmount)->comp($minAmount) >= 0)) {
                $user = User::find($wallet->user_id);

                $transactions[] = Transaction::create([
                    'user_id' => $wallet->user_id,
                    'email' => $user->email,
                    'currency' => $blockchainTx->coin,
                    'amount' => $actualAmount,
                    'txid' => $blockchainTx->txid,
                    'to_address' => $entry->address,
                    'confirmations' => $blockchainTx->confirmations,
                    'type' => Transaction::TYPE_DEPOSIT,
                    'status' => $this->getStatus($blockchainTx),
                    'is_confirm' => $this->getIsConfirm($blockchainTx),
                    'created_at' => Utils::currentMilliseconds(),
                    'updated_at' => Utils::currentMilliseconds(),
                ]);
            }
        }
        return $transactions;
    }

    public function getMinDepositAmount($coin)
    {
        switch ($coin) {
            case 'eth':
                return system_setting(Consts::MIN_DEPOSIT_ETH, 0);
            case 'btc':
                return system_setting(Consts::MIN_DEPOSIT_BTC, 0);
            case 'usdt':
                return system_setting(Consts::MIN_DEPOSIT_USDT, 0);
            default:
                return system_setting(Consts::MIN_DEPOSIT_ETH, 0);
        }
    }

    private function updateTransactions($blockchainTx)
    {
        $transactions = [];
        foreach ($blockchainTx->entries as $entry) {
            if ($entry->value > 0) {
                $transaction = Transaction::where('txid', $blockchainTx->txid)
                ->where('currency', $blockchainTx->coin)
                ->where('to_address', $entry->address)
                ->lockForUpdate()
                ->first();

                if ($transaction && (!$transaction->is_confirm) && ($transaction->confirmations !== $blockchainTx->confirmations)) {
                    $transaction->confirmations = $blockchainTx->confirmations;
                    $transaction->status = $this->getStatus($blockchainTx);
                    $transaction->is_confirm = $this->getIsConfirm($blockchainTx);
                    $transaction->updated_at = Utils::currentMilliseconds();
                    $transaction->save();
                    $transactions[] = $transaction;
                }
            }
        }
        return $transactions;
    }

    private function getStatus($blockchainTx)
    {
        $settingKey = $this->getConfirmationKey($blockchainTx->coin);
        $expectedConfirmations = system_setting($settingKey);
        return $blockchainTx->confirmations >= $expectedConfirmations ? Transaction::STATUS_SUCCEEDED : Transaction::STATUS_CONFIRMING;
    }

    private function getActualValue($currency, $value)
    {
        $precisions = ['eth' => 18, 'btc' => 8, 'usdt' => 0];
        return BigNumber::new($value)->div("1e{$precisions[$currency]}")->toString();
    }

    private function getConfirmationKey($coin)
    {
        switch ($coin) {
            case 'eth':
                return Consts::CONFIRMATION_ETH;
            case 'btc':
                return Consts::CONFIRMATION_BTC;
            case 'usdt':
                return Consts::CONFIRMATION_USDT;
            default:
                return Consts::CONFIRMATION_ETH;
        }
    }

    private function getIsConfirm($blockchainTx)
    {
        return $this->getStatus($blockchainTx) === Transaction::STATUS_SUCCEEDED;
    }

    private function getWallet($coin, $address)
    {
        return Wallet::where('currency', $coin)->where('address', $address)->first();
    }

    private function dropBlockchainTransaction($coin, $transactionId)
    {
        return Transaction::where('txid', $transactionId)
            ->where('currency', $coin)
            ->update(['status' => Transaction::STATUS_FAILED]);
    }
}
