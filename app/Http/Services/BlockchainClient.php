<?php

namespace App\Http\Services;

use GuzzleHttp\Client;

class BlockchainClient
{
    public function generateAddress($coin)
    {
        $coin = $coin === 'usdt' ? 'btc' : $coin;
        $requestPath = "/$coin/address";
        $response = $this->requestApi('POST', $requestPath);
        $address = json_decode($response->getBody()->getContents());

        logger()->info("The {$coin} address {$address->address} was generated");
        return $address->address;
    }

    public function getBlockchainTransaction($coin, $transactionId)
    {
        $requestPath = "/$coin/tx/$transactionId";

        $response = $this->requestApi('GET', $requestPath);

        $body = $response->getBody();
        $transaction = json_decode($body->getContents());

        logger()->info("Transaction info: id: $transactionId, coin: $coin, content: " . json_encode($transaction));
        return $transaction;
    }

    protected function requestApi($method, $path)
    {
        $client = new Client(['verify' => false]);
        return $client->request($method, env('BLOCKCHAIN_SERVICE_URL') . $path);
    }
}