<?php

namespace App\Http\Services;

use App\BigNumber;
use App\Events\BalanceUpdated;
use App\Models\Transaction;
use App\Models\Conversion;
use App\Models\Wallet;
use App\Models\User;
use Carbon\Carbon;
use App\Consts;
use Auth;
use DB;

class WalletService
{
    private $blockchainClient;

    function __construct(BlockchainClient $blockchainClient)
    {
        $this->blockchainClient = $blockchainClient;
    }

    public function getTransactions($params)
    {
        $userId = Auth::user()->id;

        $query = Transaction::when(!empty($params['currency']), function($query) use ($params){
            $query->where('currency', $params['currency']);
        })->where('user_id', $userId);

        if (!empty($params['start_at'])) {
            $query->where('created_at', '>=', $params['start_at']);
        }

        if (!empty($params['end_at'])) {
            $query->where('created_at', '<', $params['end_at']);
        }

        if (!empty($params['type'])) {
            $query->where('type', $params['type']);
        }

        $query->when(!empty($params['sort_field']), function ($query) use ($params) {
            $query->orderBy($params['sort_field'], $params['sort_direction']);
        }, function($query) {
            $query->orderBy('created_at', 'desc');
        });

        return $query->get();
    }

    public function generateDepositAddress(User $user, $coin)
    {
        $wallet = Wallet::where('user_id', $user->id)->where('currency', $coin)->first();
        if ($wallet && $wallet->address) {
            return $wallet;
        }
        return $this->createWallet($user, $coin);
    }

    private function createWallet(User $user, $coin)
    {
        $address = $this->blockchainClient->generateAddress($coin);
        if (Wallet::where('address', $address)->where('currency', $coin)->exists()) {
            return $this->createWallet($user, $coin);
        };

        $wallet = Wallet::updateOrCreate(
            ['user_id' => $user->id, 'currency' => $coin],
            ['address' => $address]
        );

        $this->createSameAddressWallet($user, $coin, $address);

        return Wallet::find($wallet->id);
    }

    private function createSameAddressWallet(User $user, $coin, $address)
    {
        $coin = $coin === 'usdt' ? 'btc' : ($coin === 'btc' ? 'usdt' : null);
        if ($coin) {
            Wallet::updateOrCreate(
                ['user_id' => $user->id, 'currency' => $coin],
                ['address' => $address]
            );
        }
    }

    public function getSpentVsDepositedStats($params) {
        $conversions = Conversion::select('currency', DB::raw('sum(currency_amount) as currency_amount, sum(bonus_amount) as bonus_amount, sum(token_amount) as token_amount'))
            ->when(!empty($params['start_at']), function($query) use ($params) {
                return $query->where('created_at', '>=', $params['start_at']);
            })
            ->when(!empty($params['end_at']), function($query) use ($params) {
                return $query->where('created_at', '<=', $params['end_at']);
            })
            ->groupBy('currency')
            ->get();

        $deposits = Transaction::select('currency', DB::raw('sum(amount) as amount'))
            ->when(!empty($params['start_at']), function($query) use ($params) {
                return $query->where('created_at', '>=', $params['start_at']);
            })
            ->when(!empty($params['end_at']), function($query) use ($params) {
                return $query->where('created_at', '<=', $params['end_at']);
            })
            ->where('type', Transaction::TYPE_DEPOSIT)
            ->where('status', Transaction::STATUS_SUCCEEDED)
            ->groupBy('currency')
            ->get();

        $result = [
            $this->extractSpentVsDeposit('btc', $conversions, $deposits),
            $this->extractSpentVsDeposit('eth', $conversions, $deposits),
            $this->extractSpentVsDeposit('usdt', $conversions, $deposits)
        ];
        return $result;
    }

    private function extractSpentVsDeposit($currency, $conversions, $deposits) {
        $result = ['currency' => $currency];
        $sum = $conversions->firstWhere('currency', $currency);
        $tokenAmount = $sum ? $sum->token_amount : 0;
        $bonusAmount = $sum ? $sum->bonus_amount : 0;
        $result['token_usd_amount'] = BigNumber::new($tokenAmount)->mul(system_setting(Consts::ICO_PRICE_HETA_TOKEN_PER_USD))->toString();
        $result['bonus_usd_amount'] = BigNumber::new($bonusAmount)->mul(system_setting(Consts::ICO_PRICE_HETA_TOKEN_PER_USD))->toString();
        $depositedAmount = $deposits->firstWhere('currency', $currency);
        $depositedAmount = $depositedAmount ? $depositedAmount->amount : 0;
        $boughtAmount = $sum ? $sum->currency_amount : 0;
        $result['currency_usd_amount'] = BigNumber::new($depositedAmount)->sub($boughtAmount)->toString();

        $result['currency_amount'] = $boughtAmount;
        $result['token_amount'] = $tokenAmount;
        $result['bonus_amount'] = $bonusAmount;
        return $result;
    }

    public function updateUserWalletBalance($userId, $currency, $amount, $updateBalance = true, $updateAvailableBalance = true)
    {
        $bindings = [];
        $query = 'update wallets set ';
        if ($updateBalance) {
            $query .= 'balance = balance + ?, ';
            $bindings[] = $amount;
        }

        if ($updateAvailableBalance) {
            $query .= 'available_balance = available_balance + ?, ';
            $bindings[] = $amount;
        }

        $query .= 'updated_at = ? where user_id = ? and currency = ?';
        array_push($bindings, Carbon::now(), $userId, $currency);

        DB::statement($query, $bindings);

        $wallet = Wallet::where('user_id', $userId)->where('currency', $currency)->first();
        event(new BalanceUpdated($userId, $wallet));
        return $wallet;
    }

    private function buildQueryDistributionHetaTokens($params)
    {
        return User::join('wallets', function ($join) {
            $join ->on('wallets.user_id', 'users.id')
                ->where('currency', Consts::HETA_TOKEN)
                ->where(function ($query) {
                    $query->where('available_balance', '>', 0)
                        ->orWhere('bounty_amount', '>', 0);
                });
        })->when(!empty($params['search']), function($query) use ($params) {
            return $query->where('users.email', 'like', "%{$params['search']}%");
        })
        ->when(!empty($params['sortField']) && !empty($params['sortDirection']), function ($query) use ($params) {
            $query->orderBy($params['sortField'], $params['sortDirection']);
        });
    }

    public function getDistributionHetaTokens($params)
    {
        $limit = array_get($params, 'limit', Consts::DEFAULT_PER_PAGE);
        $data = $this->buildQueryDistributionHetaTokens($params)->paginate($limit, ['users.email', 'users.id as user_id',
            'users.kyc_status', 'wallets.balance', 'wallets.available_balance', 'wallets.bounty_amount', 'wallets.approve_status']);

        return $data->getCollection()->each(function ($item) {
            $item->withdraw_amount = 0;
            if ($item->approve_status) {
                $item->withdraw_amount = $item->bounty_amount;
            }

           if ($item->kyc_status === User::KYC_STATUS_VERIFIED) {
               $item->withdraw_amount = BigNumber::new($item->withdraw_amount)->add($item->available_balance)->toString();
           }
        });
    }

    public function exportDistributionHetaTokens($params)
    {
        $query = $this->buildQueryDistributionHetaTokens($params)->select('users.email', 'users.id as user_id',
            'users.kyc_status', 'wallets.balance', 'wallets.available_balance', 'wallets.bounty_amount', 'wallets.approve_status');

        $data = collect([]);
        foreach ($query->cursor() as $row) {
            $row->withdraw_amount = 0;
            if ($row->approve_status) {
                $row->withdraw_amount = $row->bounty_amount;
            }

            if ($row->kyc_status === User::KYC_STATUS_VERIFIED) {
                $row->withdraw_amount = BigNumber::new($row->withdraw_amount)->add($row->available_balance)->toString();
            }

            if (BigNumber::new($row->withdraw_amount)->comp(0) > 0) {
                $data->push($row);
            }
        }

        return $data;
    }

    public function approveBountyWithdraw($params)
    {
        Wallet::where('user_id', array_get($params, 'user_id'))->where('currency', Consts::HETA_TOKEN)->update([
            'approve_status' => true
        ]);
        return 'ok';
    }
}
