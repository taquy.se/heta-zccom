<?php

namespace App\Http\Services;

use App\Consts;
use App\Models\Transaction;
use App\Models\UserSetting;
use App\Models\BountySetting;
use App\Models\Wallet;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\VerifyUser;
use App\Models\UserKyc;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use App\Notifications\VerifyEmail;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\BigNumber;

class UserService
{
    public function getCurrentUser(Request $request)
    {
        $user = User::find($request->user()->id);
        return $user;
    }

    public function getReferralProgramRecords(Request $request)
    {
        $limit = $request->input('limit', Consts::DEFAULT_PER_PAGE);

        $records = $request->user()->bounties()->referralRecords()->approved()->paginate($limit);

        $records->getCollection()->each(function ($record) {
            $record->meta_data = $this->hidePartOfEmail($record->meta_data);
        });

        return $records;
    }

    private function hidePartOfEmail($email)
    {
        $ext = strrev(array_first(explode(".", strrev($email)), null, 'com'));
        return substr($email, 0, 5) . '***@***.' . $ext;
    }

    public function getEstHetaTokens()
    {
        return BountySetting::where('key', 'max_heta_tokens')->value('value');
    }

    public function getTotalHetaTokens(Request $request)
    {
        $query = $request->user()->bounties()->approved();
        $type = $request->input('type');

        switch ($type) {
            case 'referral':
                $query->where('type', 'like', 'referral%');
                break;
        }

        $total = $query->sum('heta_tokens');

        if (empty($type)) {
            $totalBugBounty = $request->user()->bugBounties()->approved()->sum('heta_tokens');
            $total = BigNumber::new($total)->add($totalBugBounty)->toString();
        }

        $maxToken = BountySetting::where('key', 'max_heta_tokens')->value('value');

        return min($total, $maxToken);
    }

    public function store(Request $request)
    {
        $referrerId = null;
        if ($request->has('referral_id') && !!$request->referral_id) {
            $referrer = User::where('referral_id', $request->referral_id)
                ->where('is_verified', true)
                ->firstOrFail();

            $referrerId = $referrer->id;
        }

        $user = User::firstOrNew([
            'email' => $request->email,
        ]);

        $user->fill([
            'referrer_id' => $referrerId,
            'password' => Hash::make($request->password),
        ]);
        $user->save();

        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
        ]);

        $user->notify(new VerifyEmail($user));
        return $user;
    }

    public function resetPassword(Request $request)
    {
        $this->broker()->sendResetLink(
            $request->only('email')
        );
    }

    public function submitResetPassword(Request $request)
    {
        $this->broker()->resetPassword(
            $request, function ($user, $password) {
                $user->password = Hash::make($password);
                $user->save();
            }
        );
    }

    public function isValidResetPasswordLink(Request $request)
    {
        return $this->broker()->isValidLink($request->all());
    }

    protected function broker()
    {
        return Password::broker();
    }

    public function changePassword(Request $request)
    {
        $user = User::find($request->user()->id);
        $user->password = Hash::make($request->password);
        $user->save();

        Auth::logout();
        $request->session()->invalidate();
    }

    public function updateUserInfoAfterKyc(User $user)
    {
        $client = new Client();
        $host = env('SUM_SUB_HOST');
        $key = env('SUM_SUB_API_KEY');

        $url = "{$host}/resources/applicants/{$user->userKyc->applicant_id}?key={$key}";

        $response = $client->get($url);
        $data = json_decode($response->getBody()->getContents());
        $info = $data->list->items[0]->info;

        $name = "{$info->firstName} {$info->lastName}";
        $user->kyc_status = User::KYC_STATUS_PENDING;
        $user->name = $name;
        $user->save();

        return $user;
    }

    public function getUserReport(Request $request)
    {
        $startDate = Carbon::parse($request->startDate);
        $endDate = Carbon::parse($request->endDate);

        $registerCount = User::where('role', User::USER_ROLE)->whereBetween('created_at', [
            $startDate->startOfDay()->format('Y-m-d H:i:s'),
            $endDate->endOfDay()->format('Y-m-d H:i:s'),
        ])->count();

        $kycCount = UserKyc::whereBetween('created_at', [
            $startDate->startOfDay()->format('Y-m-d H:i:s'),
            $endDate->endOfDay()->format('Y-m-d H:i:s'),
        ])->count();

        return [
            'registerCount' => $registerCount,
            'kycCount' => $kycCount,
        ];
    }

    public function changeAddress(Request $request)
    {
        $user = $request->user();
        $currency = $request->currency;
        $address = $request->address;

        $user->update(["{$currency}_address" => $address]);

        $user->addressHistories()->create(['currency' => $currency, 'address' => $address]);

        return $user;
    }

    public function updateUserLocale($locale)
    {
        if (!Auth::check()) return $locale;

        UserSetting::updateOrCreate(
            ['user_id' => Auth::id(), 'key' => 'locale'],
            ['value' => $locale]);

        return $locale;
    }

    public function getCurrentUserLocale()
    {
        if (!Auth::check()) return Session::get('user.locale', App::getLocale());

        $userLocale = UserSetting::where('user_id', Auth::id())->where('key', 'locale')->value('value');
        return $userLocale ?? App::getLocale();
    }

    public function getKYCStatistics()
    {
        $kycAllUsers = User::where('role', User::USER_ROLE)
            ->select('kyc_status', DB::raw('count(*) as number'))
            ->groupBy('kyc_status')->get();

        $depositedUserIds = Transaction::where('type', Transaction::TYPE_DEPOSIT)->where('status', Transaction::STATUS_SUCCEEDED)->distinct('user_id')->pluck('user_id');
        $kycDepositedUsers = User::where('role', User::USER_ROLE)
            ->select('kyc_status', DB::raw('count(*) as number'))
            ->whereIn('id', $depositedUserIds)
            ->groupBy('kyc_status')->get();

        return ['all' =>  $kycAllUsers, 'deposited' => $kycDepositedUsers];
    }
}
