<?php

namespace App\Http\Services;

use App\Consts;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Cache;
use App\Exceptions\BusinessException;
use App\Utils;
use App\BigNumber;
use App\Models\Conversion;
use App\Models\ConversionSetting;
use App\Models\Bonus;
use App\Models\Wallet;
use App\Jobs\FluctuateHetaBalance;
use App\Events\ConversionCreated;
use Carbon\Carbon;
use DB;

class ConversionService
{
    function __construct(PriceService $priceService, WalletService $walletService)
    {
        $this->priceService = $priceService;
        $this->walletService = $walletService;
    }

    public function conversionSetting()
    {
        return ConversionSetting::all();
    }

    public function getUserConversions($userId) {
        return Conversion::where('user_id', $userId)->orderBy('id', 'desc')->get();
    }

    public function create($input)
    {
        $now = Utils::currentMilliseconds();
        $this->checkICODuration($now);

        $conversionSetting = $this->conversionSetting()->first(function($value) use ($input) {
            return $value->currency == $input['currency'];
        });
        $precision = $conversionSetting->currency_precision;
        $input['currency_to_usd_price'] = $this->priceService->getUSDPrice($input['currency']);
        $input['token_to_currency_price'] = $this->priceService->getHetaPrice($input['currency'], $precision);

        $this->validateOrderAttribute($input, 'token_amount', $conversionSetting->token_precision, BigNumber::new($conversionSetting->min_token)->toString());

        $currencyAmount = BigNumber::new($input['token_to_currency_price'])->mul($input['token_amount'])->toString();
        $input['currency_amount'] = BigNumber::round($currencyAmount, BigNumber::ROUND_MODE_CEIL, $precision);

        $this->validateCurrencyAmount($input, $conversionSetting);

        $input['created_at'] = $now;
        $input['updated_at'] = $now;

        $input['bonus_percent'] = $this->getBonusRate($now);
        $input['bonus_amount'] = $this->calculateBonusAmount($input, $conversionSetting->token_precision);

        $input['total_token_amount'] = BigNumber::new($input['token_amount'])->add($input['bonus_amount'])->toString();

        $input['created_day_london'] = Carbon::now(1); //always use gmt + 1 instead of london

        $conversion = Conversion::create($input);
        $this->updateBalanceForNewOrder($conversion);

        event(new ConversionCreated($conversion->user_id, $conversion));

        return $conversion;
    }

    private function checkICODuration($now) {
        if (Utils::lessThan($now, system_setting(Consts::ICO_START_TIME))) {
            throw new BusinessException(__('errors.ico_not_started'));
        }
        if (Utils::lessThan(system_setting(Consts::ICO_END_TIME), $now)) {
            throw new BusinessException(__('errors.ico_ended'));
        }
    }

    private function updateBalanceForNewOrder($conversion) {
        //decrease currency balance
        $decreasedAmount = BigNumber::new($conversion->currency_amount)->mul(-1)->toString();
        $this->walletService->updateUserWalletBalance($conversion->user_id, $conversion->currency, $decreasedAmount);

        //increase heta token balance
        $actualAmount = BigNumber::new($conversion->token_amount)->add($conversion->bonus_amount)->toString();
        $wallet = $this->walletService->updateUserWalletBalance($conversion->user_id, 'heta', $actualAmount);

        FluctuateHetaBalance::dispatchNow($conversion, $wallet->balance);
    }

    private function calculateBonusAmount($input, $tokenPrecision) {
        $bonusAmount = BigNumber::new($input['token_amount'])->mul($input['bonus_percent']);
        return BigNumber::round($bonusAmount, BigNumber::ROUND_MODE_FLOOR, $tokenPrecision);
    }

    private function getBonusRate($now) {
        $bonus = Bonus::where('timestamp', '>=', $now)->orderBy('timestamp', 'asc')->first();
        if ($bonus) {
            return $bonus->percent;
        }
        return 0;
    }

    private function validateCurrencyAmount($input, $conversionSetting) {
        // $minCurrencyAmount = BigNumber::new($conversionSetting->min_currency)->toString();
        // $this->validateMinValue('currency_amount', $input['currency_amount'], $minCurrencyAmount);

        $currentBalance = Wallet::where('user_id', $input['user_id'])->where('currency', $input['currency'])->lockForUpdate()->value('available_balance');
        if (Utils::lessThan($currentBalance, $input['currency_amount'])) {
            throw ValidationException::withMessages(['currency_amount' => [__('validation.insufficient_balance')]]);
        }
    }

    private function validateMinValue($attribute, $value, $min) {
        if (Utils::lessThan($value, $min)) {
            throw ValidationException::withMessages([$attribute => [__('validation.min.numeric', ['attribute' => __($attribute), 'min' => $min])]]);
        }
    }

    private function validateOrderAttribute($input, $attribute, $precision, $min) {
        $value = $input[$attribute];
        $this->validateMinValue($attribute, $value, $min);

        $precision = intval($precision);

        if (!$this->isMultiple($value, $precision)) {
            if ($precision === 0) {
                throw ValidationException::withMessages([$attribute => [__('validation.integer', ['attribute' => __($attribute)])]]);
            }
            throw ValidationException::withMessages([$attribute => [__('validation.precision', ['attribute' => __($attribute), 'precision' => $precision])]]);
        }
    }

    private function isMultiple($value, $precision) {
        $base = BigNumber::new(1)->div(pow(10, $precision));
        $ratio = BigNumber::new($value)->div($base)->toString();
        return $ratio === BigNumber::new(round($ratio))->toString();
    }

    public function getConversionStatistics($params)
    {
        $limit = array_get($params, 'limit', Consts::DEFAULT_PER_PAGE);

        return Conversion::select('created_day_london',
            DB::raw("SUM(CASE WHEN `currency` = 'btc' THEN currency_amount END) as total_btc_amount,
                           SUM(CASE WHEN `currency` = 'eth' THEN currency_amount END) as total_eth_amount,
                           SUM(CASE WHEN `currency` = 'usdt' THEN currency_amount END) as total_usdt_amount,
                           SUM(`token_amount`) as token_amount, 
                           SUM(`bonus_amount`) as bonus_amount"
            ))
            ->when(!empty($params['start_at']), function($query) use ($params) {
                return $query->where('created_at', '>=', $params['start_at']);
            })
            ->when(!empty($params['end_at']), function($query) use ($params) {
                return $query->where('created_at', '<=', $params['end_at']);
            })
            ->groupBy('created_day_london')
            ->orderBy('created_day_london', 'desc')
            ->paginate($limit);
    }

    public function getSoldTokenCount() {
        $result = [];
        $bonuses = Bonus::orderBy('timestamp')->get();
        $startTime = $endTime = 0;
        $total = BigNumber::new(0);
        $now = Utils::currentMilliseconds();
        foreach ($bonuses as $bonus) {
            $endTime = $bonus->timestamp;
            $result[$endTime] = $sold = $this->getTokensSoldInDuration($startTime, $endTime, $now);
            $total = $total->add($sold);
            $startTime = BigNumber::new($endTime)->add(1)->toString();
        }

        $total->add($this->getTokensSoldInDuration($endTime, $now, $now));
        $result['total'] = $total->toString();
        return $result;
    }

    private function getTokensSoldInDuration($startTime, $endTime, $now) {
        if (Utils::lessThan($now, $startTime)) { //future
            return 0;
        }
        if (Utils::lessThan($endTime, $now)) { //already passed
            $key = "TokenSold:$startTime:$endTime";
            if (Cache::has($key)) {
                return Cache::get($key);
            }
            $tokens = Conversion::where('created_at', '>=', $startTime)->where('created_at', '<=', $endTime)->sum('total_token_amount');
            Cache::forever($key, $tokens);
            return $tokens;
        }
        return Conversion::where('created_at', '>=', $startTime)->where('created_at', '<=', $endTime)->sum('total_token_amount');
    }
}
