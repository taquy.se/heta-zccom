<?php

namespace App\Http\Services;
use App\Events\PricesUpdated;
use App\Models\Price;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use App\BigNumber;

class PriceService
{
    private $convertTo = 'USD';

    public function crawler()
    {
        $client = new Client();
        $promises = [
            'btc'       => $client->getAsync('https://api.coinmarketcap.com/v2/ticker/1'),
            'eth'       => $client->getAsync('https://api.coinmarketcap.com/v2/ticker/1027'),
            'usdt'      => $client->getAsync('https://api.coinmarketcap.com/v2/ticker/825'),
        ];

        $results = Promise\unwrap($promises);

        $priceUpdated = [];

        foreach ($results as $result) {
            $data = json_decode($result->getBody()->getContents())->data;
            $newPrice = $data->quotes->{$this->convertTo}->price;
            $coin = strtolower($data->symbol);

            $price = Price::updateOrCreate(['coin' => $coin], ['price' => $newPrice]);

            $priceUpdated[$price->coin] = $price->price;
        }

        event(new PricesUpdated($priceUpdated));
    }

    public function getPrices()
    {
        $prices = Price::all()->mapWithKeys(function ($price) {
            return [$price['coin'] => $price->price];
        });
        $prices['heta'] = system_setting('ico_price_heta_token_per_usd');
        return $prices->toArray();
    }

    public function getUSDPrice($currency) {
        return $this->getPrices()[$currency];
    }

    public function getHetaPrice($currency, $precision) {
        $currencyPrice = $this->getPrices()[$currency];
        $hetaUSDPrice = system_setting('ico_price_heta_token_per_usd');
        $price = BigNumber::new($hetaUSDPrice)->div($currencyPrice)->toString();
        return BigNumber::round($price, BigNumber::ROUND_MODE_CEIL, $precision);
    }
}
