<?php

namespace App\Exports;

use App\Models\Transaction;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\Services\WalletService;
use Carbon\Carbon;
use App\Utils;

class TransactionsExport implements FromCollection, WithMapping, WithHeadings
{
    function __construct(WalletService $walletService, $params)
    {
        $this->params = $params;
        $this->walletService = $walletService;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->walletService->getTransactions($this->params);
    }

    public function map($transaction): array
    {
        return [
            $transaction->status,
            $transaction->currency,
            abs($transaction->amount),
            Utils::millisecondsToDateTime($transaction->created_at, $this->params['timezone_offset'], 'Y-m-d H:i:s'),
            $transaction->to_address,
            $transaction->txid
        ];
    }

    public function headings(): array
    {
        return [__('wallet.transaction.status'), __('wallet.transaction.coin'), __('wallet.transaction.amount'), __('wallet.transaction.date'), __('wallet.transaction.address'), __('wallet.transaction.txid')];
    }
}
