<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\Services\WalletService;
use App\Utils;

class DistributionHetaTokensExport implements FromCollection, WithMapping, WithHeadings
{
    protected $params;
    protected $walletService;

    function __construct(WalletService $walletService, $params)
    {
        $this->params = $params;
        $this->walletService = $walletService;
    }

    /**
     * @return array
     */
    public function collection()
    {
        return $this->walletService->exportDistributionHetaTokens($this->params);
    }

    public function map($row): array
    {
        return [
            $row->email,
            $row->buy_amount,
            $row->bounty_amount,
            $row->withdraw_amount,
        ];
    }

    public function headings(): array
    {
        return [__('wallet.distribution.user'), __('wallet.distribution.buy_amount'), __('wallet.distribution.bounty_amount'), __('wallet.distribution.withdraw_amount')];
    }
}
