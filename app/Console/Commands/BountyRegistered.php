<?php

namespace App\Console\Commands;

use App\Jobs\BountyApproved;
use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Bounty;

class BountyRegistered extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bounty:registered';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->approvePendingBounty();
        $this->createRegisteredBounty();
    }

    private function approvePendingBounty()
    {
        $bounties = Bounty::join('users', function ($join) {
            $join->where('users.is_verified', true)
                ->where('users.role', User::USER_ROLE)
                ->on('users.id', '=', 'bounties.user_id');
        })
            ->where(function ($query) {
                $query->where('bounties.type', Bounty::REGISTER_TYPE)
                    ->orWhere('bounties.type', Bounty::REFERRAL_REGISTER_TYPE);
            })->where('bounties.status', Bounty::STATUS_PENDING)
            ->select('bounties.*')->get();

        foreach ($bounties as $bounty) {
            $bounty->status = Bounty::STATUS_APPROVED;

            $bounty->heta_tokens = $bounty->bountySetting->value ?? 0;

            if ($bounty->type === Bounty::VOUCHER_TYPE) {
                $bounty->heta_tokens = $bounty->voucher->token ?? 0;
            }

            $bounty->save();
            BountyApproved::dispatch($bounty);
        }
    }

    private function createRegisteredBounty()
    {
        $users = User::where('is_verified', true)->where('role', User::USER_ROLE)
            ->whereDoesntHave('bounties', function ($query) {
                $query->where('type', Bounty::REGISTER_TYPE);
        })->select('users.*')->get();
        foreach ($users as $user) {
            $userBounty = $user->bounties()->create([
                'type' => Bounty::REGISTER_TYPE,
                'status' => Bounty::STATUS_APPROVED,
            ]);
            BountyApproved::dispatch($userBounty);

            $referrer = $user->referrer;
            if ($referrer) {
                $referrerBounty = $referrer->bounties()->create([
                    'type' => Bounty::REFERRAL_REGISTER_TYPE,
                    'meta_data' => $user->email,
                    'status' => Bounty::STATUS_APPROVED
                ]);
                BountyApproved::dispatch($referrerBounty);
            }
        }
    }
}
