<?php

namespace App\Console\Commands;

use App\Http\Services\PriceService;
use Illuminate\Console\Command;
use Log;
use DB;
use Exception;

class PriceCrawler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'price:crawler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param PriceService $priceService
     * @return mixed
     */
    public function handle(PriceService $priceService)
    {
        DB::beginTransaction();
        try {
            $priceService->crawler();
            DB::commit();
        } catch(Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
        }
    }
}
