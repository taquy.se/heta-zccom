<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Wallet;
use App\Events\BalanceUpdated as BalanceUpdatedEvent;

class CreateWallets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:wallets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create wallets for registed users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currencies = ['btc', 'eth', 'usdt', 'heta'];
        $users = User::where('role', '<>', User::ADMIN_ROLE)->with('wallets')->get();

        $this->info('Creating wallets for registed users');

        $bar = $this->output->createProgressBar(count($users));

        foreach ($users as $user) {
            $bar->advance();

            $createdWallets = $user->wallets->pluck('currency')->all();
            foreach ($currencies as $currency) {
                if (!in_array($currency, $createdWallets)) {
                    Wallet::create(['user_id' => $user->id, 'currency' => $currency]);
                }
            }
        }

        $bar->finish();
        $this->info("\nFinished");
    }
}
