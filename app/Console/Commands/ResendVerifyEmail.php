<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Notifications\VerifyEmail;

class ResendVerifyEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resend:email';

    const TIME_RESEND = '2018-10-16 11:35:21';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('created_at', '>=', self::TIME_RESEND)->where('is_verified', false)->get();
        $users->each(function ($user) {
            $user->notify(new VerifyEmail($user));
        });
    }
}
