<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\Bounty;

class BountyConvertCreatedAt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bounty:convert_created_at';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Bounty::chunk(100, function ($bounties) {
            foreach ($bounties as $bounty) {
                $updateAt = Carbon::parse($bounty->updated_at);
                $bounty->created_at = $updateAt->timestamp*1000;
                $bounty->save();
            }
        });
    }
}
