<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\PricesUpdated as PricesUpdatedEvent;

class PricesUpdated extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prices:updated {data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'prices updated';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = $this->argument('data');
        event(new PricesUpdatedEvent($data));
    }
}
