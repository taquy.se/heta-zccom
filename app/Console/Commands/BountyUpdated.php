<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\BountyUpdated as BountyUpdatedEvent;

class BountyUpdated extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bounty:updated {userId} {data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'bounty updated';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = $this->argument('userId');
        $data = $this->argument('data');
        event(new BountyUpdatedEvent($userId, $data));
    }
}
