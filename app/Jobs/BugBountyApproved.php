<?php

namespace App\Jobs;

use App\Models\BugBounty;
use App\Jobs\BountyApproved;

class BugBountyApproved extends BountyApproved
{
    /**
     * Create a new job instance.
     *
     * @param Bounty $bounty
     */
    public function __construct(BugBounty $bounty)
    {
        $this->bounty = $bounty;
    }
}
