<?php

namespace App\Jobs;

use App\Models\Conversion;
use App\Models\BugBounty;
use App\Models\HetaFluctuation;
use App\Events\HetaBalanceFluctuated;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;
use Carbon\Carbon;
use App\Utils;

class FluctuateHetaBalance implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $action;
    private $endingBalance;

    public function __construct($action, $endingBalance)
    {
        $this->action = $action;
        $this->endingBalance = $endingBalance;
    }

    public function handle()
    {
        if ($this->isConversion($this->action)) {
            $data = $this->parseConversion($this->action, $this->endingBalance);
        } else if ($this->isBugBounty($this->action)) {
            $data = $this->parseBugBounty($this->action, $this->endingBalance);
        } else {
            $data = $this->parseBounty($this->action, $this->endingBalance);
        }

        $hetaFluctuation = HetaFluctuation::create($data);

        event(new HetaBalanceFluctuated($hetaFluctuation->user_id, $hetaFluctuation));
    }

    private function parseConversion($conversion, $endingBalance)
    {
        return [
            'user_id' => $conversion->user_id,
            'ending_balance' => $endingBalance,
            'action_id' => $conversion->id,
            'action_type' => HetaFluctuation::CONVERSION_ACTION_TYPE,
            'amount' => $conversion->total_token_amount,
            'object' => $conversion->currency_amount,
            'object_type' => $conversion->currency,
            'created_at' => Utils::currentMilliseconds(),
            'updated_at' => Utils::currentMilliseconds(),
        ];
    }

    private function parseBounty($bounty, $endingBalance)
    {
        return [
            'user_id' => $bounty->user_id,
            'ending_balance' => $endingBalance,
            'action_id' => $bounty->id,
            'action_type' => HetaFluctuation::BOUNTY_ACTION_TYPE,
            'amount' => $bounty->heta_tokens,
            'object' => $bounty->meta_data,
            'object_type' => $bounty->type,
            'created_at' => Utils::currentMilliseconds(),
            'updated_at' => Utils::currentMilliseconds(),
        ];
    }

    private function parseBugBounty($bounty, $endingBalance)
    {
        return [
            'user_id' => $bounty->user_id,
            'ending_balance' => $endingBalance,
            'action_id' => $bounty->id,
            'action_type' => HetaFluctuation::BOUNTY_ACTION_TYPE,
            'amount' => $bounty->heta_tokens,
            'object' => $bounty->title,
            'object_type' => BugBounty::BUG_REPORT_TYPE,
            'created_at' => Utils::currentMilliseconds(),
            'updated_at' => Utils::currentMilliseconds(),
        ];
    }

    private function isConversion($action)
    {
        return $action instanceof Conversion;
    }

    private function isBugBounty($action)
    {
        return $action instanceof BugBounty;
    }
}
