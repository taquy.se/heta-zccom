<?php

namespace App\Jobs;

use App\BigNumber;
use App\Consts;
use App\Events\BalanceUpdated;
use App\Http\Services\WalletService;
use App\Models\Bounty;
use App\Models\BountySetting;
use App\Models\SystemSetting;
use App\Models\Wallet;
use App\Utils;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;

class BountyApproved implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $bounty;

    /**
     * Create a new job instance.
     *
     * @param Bounty $bounty
     */
    public function __construct(Bounty $bounty)
    {
        $this->bounty = $bounty;
    }

    /**
     * Execute the job.
     *
     * @param WalletService $walletService
     * @return void
     * @throws Exception
     */
    public function handle(WalletService $walletService)
    {
        $endTimeIco = system_setting(Consts::ICO_END_TIME);
        $nowTime = Utils::currentMilliseconds();

        if (Utils::lessThan($endTimeIco, $nowTime)) {
            return;
        }

        DB::beginTransaction();
        try {
            //TODO limit bounty max
            //TODO set retry time on queue

            $wallet = Wallet::where('user_id', $this->bounty->user_id)->where('currency', Consts::HETA_TOKEN)->lockForUpdate()->first();

            $maxToken = BountySetting::where('key', 'max_heta_tokens')->value('value');
            if (BigNumber::new($wallet->bounty_amount)->comp($maxToken) >= 0) {
                throw new Exception("Error userId {$this->bounty->user_id} exceed max token");
            }

            $hetaTokens = $this->bounty->heta_tokens;
            $remainingHetaTokens = BigNumber::new($maxToken)->sub($wallet->bounty_amount)->toString();
            if (BigNumber::new($remainingHetaTokens)->comp($this->bounty->heta_tokens) < 0) {
                $hetaTokens = $remainingHetaTokens;
            }

            // update bounty amount
            Wallet::where('user_id', $this->bounty->user_id)->where('currency', Consts::HETA_TOKEN)->increment('bounty_amount', $hetaTokens);
            $wallet = $walletService->updateUserWalletBalance($this->bounty->user_id, Consts::HETA_TOKEN, $hetaTokens);

            FluctuateHetaBalance::dispatchNow($this->bounty, $wallet->balance);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());

            throw $e;
        }
    }
}
