<?php

namespace App\Support;


use Illuminate\Session\SessionManager as BaseSessionManager;

class SessionManager extends BaseSessionManager
{
    protected function buildSession($handler)
    {
        if ($this->app['config']['session.encrypt']) {
            return $this->buildEncryptedSession($handler);
        }

        return new SessionStore($this->app['config']['session.cookie'], $handler);
    }
}
