<?php

namespace App\Support;

use Illuminate\Session\Store;
use Illuminate\Support\Facades\Auth;

class SessionStore extends Store
{
    /**
     * Flush the session data and regenerate the ID.
     *
     * @return bool
     */
    public function invalidate()
    {
        if (!$this->isAnyGuardLoggedIn()) {
            $this->flush();

            return $this->migrate(true);
        }
    }

    /**
     * Generate a new session identifier.
     *
     * @param  bool  $destroy
     * @return bool
     */
    public function regenerate($destroy = false)
    {
        $oldSessionId = $this->getId();
        return tap($this->migrate($destroy), function () use ($oldSessionId){
            if ($this->isAnyGuardLoggedIn()) {
                $this->setId($oldSessionId);
            } else {
                $this->regenerateToken();
            }
        });
    }

    protected function isAnyGuardLoggedIn()
    {
        $configGuards = config('auth.guards');
        foreach ($configGuards as $guard => $config){
            if (Auth::guard($guard)->check()) {
                return true;
            }
        }

        return false;
    }
}
