<?php

namespace App\Listeners;

use App\Models\User;
use Laravel\Passport\Token;
use Illuminate\Support\Facades\Event;
use Laravel\Passport\Events\AccessTokenCreated;
use App\Events\UserSessionUpdatedEvent;

class RevokeOldTokens
{
    /**
     * Handle the event.
     *
     * @param  LaravelPassportEventsAccessTokenCreated  $event
     * @return void
     */
    public function handle(AccessTokenCreated $event)
    {
        $user = User::find($event->userId);
        if ($user) {
            $previousSessionId = $user->session_id;
            $user->session_id = $event->clientId;
            $user->save();
            if ($previousSessionId && $user->role === User::USER_ROLE) {
                Event::dispatch(new UserSessionUpdatedEvent($event->userId));
            }
        }

        Token::where('user_id', $event->userId)
            ->where('client_id', $event->clientId)
            ->where('id', '!=', $event->tokenId)
            ->update(['revoked' => true]);
    }
}
