<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;
use App\Consts;

class ResetPassword extends Notification implements ShouldQueue
{
    use Queueable;

    private $url;
    private $email;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($email, $url)
    {
        $this->url = $url;
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        sleep(env('EMAIL_INTERVAL', Consts::DEFAULT_EMAIL_INTERVAL));
        return (new MailMessage)
                ->subject(Lang::getFromJson('Password Reset'))
                ->markdown('emails.reset_password', [
                    'email' => $this->email,
                    'url' => $this->url,
                ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
