<?php

namespace App;

use App\Http\Services\UserService;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class Utils
{
    public static function millisecondsToDateTime($timestamp, $timezoneOffsetInMins, $format) {
        return Utils::millisecondsToCarbon($timestamp, $timezoneOffsetInMins)->format($format);
    }

    public static function millisecondsToCarbon($timestamp, $timezoneOffsetInMins) {
        return Carbon::createFromTimestampUTC(floor($timestamp/1000))->subMinutes($timezoneOffsetInMins);
    }

    public static function setLocale($request) {
        $userService = app()->make(UserService::class);
        $userLocale = $userService->getCurrentUserLocale();

        if ($request->has('lang')) {
            $locale = $request->input('lang');

            if (in_array($locale, Consts::SUPPORTED_LOCALES)) {
                Session::put('user.locale', $locale);
            }
        }
        if (Session::has('user.locale')) {
            $locale = Session::get('user.locale');
            if ($locale !== $userLocale) {
                $userLocale = $userService->updateUserLocale($locale);
            }

            if ($userLocale !== App::getLocale()) {
                App::setLocale($userLocale);
            }
            return $userLocale;
        }
    }

    public static function currentMilliseconds() {
        return round(microtime(true) * 1000);
    }

    public static function lessThan($number1, $number2) {
        return BigNumber::new($number1)->comp($number2) < 0;
    }
}
