<?php

namespace App\Libs;

use Illuminate\Auth\Passwords\PasswordBroker as BasePasswordBroker;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Closure;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class PasswordBroker extends BasePasswordBroker
{
    public function isValidLink(array $credentials)
    {
        if (is_null($user = $this->getUser($credentials))) {
            return false;
        }

        if (! $this->tokens->exists($user, $credentials['token'])) {
            return false;
        }

        return true;
    }

    public function resetPassword(Request $request, Closure $callback)
    {
        // If the responses from the validate method is not a user instance, we will
        // assume that it is a redirect and simply return it from this method and
        // the user is properly redirected having an error message on the post.
        $credentials = $request->only('email', 'password', 'token');
        $user = $this->validateReset($credentials);

        if ($user->two_factor_enabled) {
            $request->validate(['code' => "required|is_invalid_otp_code:{$user->id}"]);
        }

        if (! $user instanceof CanResetPasswordContract) {
            return $user;
        }

        $password = $credentials['password'];

        // Once the reset has been validated, we'll call the given callback with the
        // new password. This gives the user an opportunity to store the password
        // in their persistent storage. Then we'll delete the token and return.
        $callback($user, $password);

        $this->tokens->delete($user);

        return static::PASSWORD_RESET;
    }

    protected function validateReset(array $credentials)
    {
        if (is_null($user = $this->getUser($credentials))) {
            throw ValidationException::withMessages([
               'user' => __('auth.reset_password.error_user'),
            ]);
        }

        if (! $this->tokens->exists($user, $credentials['token'])) {
            throw ValidationException::withMessages([
               'token' => __('auth.reset_password.error_token'),
            ]);
        }

        return $user;
    }
}
