<?php

namespace App\Providers;

use Auth;
use Illuminate\Auth\Events\Logout;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Request;
use Laravel\Passport\Passport;
use Laravel\Passport\PassportServiceProvider as ServiceProvider;

class PassportServiceProvider extends ServiceProvider
{
    protected function deleteCookieOnLogout()
    {
        Event::listen(Logout::class, function () {
            if (Auth::guard('web')->user() && Auth::guard('admin')->user()) {
                return;
            }
            if (Request::hasCookie(Passport::cookie())) {
                Cookie::queue(Cookie::forget(Passport::cookie()));
            }
        });
    }
}
