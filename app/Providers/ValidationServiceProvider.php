<?php

namespace App\Providers;

use App\Models\User;
use App\Models\AddressHistory;
use App\Models\Voucher;
use App\Models\Bounty;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Murich\PhpCryptocurrencyAddressValidation\Validation\ETH as ETHValidator;
use Murich\PhpCryptocurrencyAddressValidation\Validation\BTC as BTCValidator;
use Carbon\Carbon;
use PHPGangsta_GoogleAuthenticator;

class ValidationServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerValidationExtends();
    }

    protected function registerValidationExtends()
    {
        $this->registerNoSpaceValidation();
        $this->registerOldPasswordValidation();
        $this->registerValidEthereumAddressValidation();
        $this->registerValidBitcoinAddressValidation();
        $this->registerValidTetherusAddressValidation();
        $this->registerEmailVerifyValidation();
        $this->registerVerifyRegisterEmailValidation();
        $this->registerVerifyPendingEmailValidation();
        $this->registerEmailResetPasswordExistsValidation();
        $this->registerVoucherExisted();
        $this->registerVoucherIsExpired();
        $this->registerVoucherIsExpired();
        $this->registerUniqueVoucher();
        $this->registerValidateOtpCode();
        $this->registerUniqueMetadataBounty();
        $this->registerUniqueCurrencyAddress();
        $this->registerSendVerifyUserValidation();
        $this->isNotTempEmail();
    }

    protected function registerOldPasswordValidation()
    {
        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, current($parameters));
        });
    }

    protected function registerNoSpaceValidation()
    {
        Validator::extend('no_space', function ($attribute, $value, $parameters, $validator) {
            return !str_contains($value, ' ');
        });
    }

    protected function registerValidEthereumAddressValidation()
    {
        Validator::extend('valid_ethereum_address', function ($attribute, $value, $parameters, $validator) {
            $validator = new ETHValidator($value);
            return $validator->validate();
        });
    }

    protected function registerValidBitcoinAddressValidation()
    {
        Validator::extend('valid_bitcoin_address', function ($attribute, $value, $parameters, $validator) {
            $validator = new BTCValidator($value);
            return $validator->validate();
        });
    }

    protected function registerValidTetherusAddressValidation()
    {
        Validator::extend('valid_tetherus_address', function ($attribute, $value, $parameters, $validator) {
            $validator = new BTCValidator($value);
            return $validator->validate();
        });
    }

    protected function registerEmailVerifyValidation()
    {
        Validator::extend('email_verify', function ($attribute, $value, $parameters, $validator) {
            $user = User::where('email', $value)->first();
            if ($user) {
                return !!$user->is_verified;
            }
            return true;
        });
    }

    protected function registerVerifyRegisterEmailValidation()
    {
        Validator::extend('verify_register_email', function ($attribute, $value, $parameters, $validator) {
            $user = User::where('email', $value)->where('is_verified', 1)->first();
            return !$user;
        });
    }

    protected function registerVerifyPendingEmailValidation()
    {
        Validator::extend('verify_pending_email', function ($attribute, $value, $parameters, $validator) {
            $user = User::where('email', $value)->where('is_verified', 0)->first();
            return !$user;
        });
    }

    protected function registerSendVerifyUserValidation()
    {
        Validator::extend('send_verify_user', function ($attribute, $value, $parameters, $validator) {
            $user = User::where('email', $value)->first();
            return $user && !$user->is_verified;
        });
    }

    protected function registerEmailResetPasswordExistsValidation()
    {
        Validator::extend('email_reset_password_exists', function ($attribute, $value, $parameters, $validator) {
            return !!User::where('email', $value)->count();
        });
    }

    protected function registerVoucherExisted()
    {
        Validator::extend('voucher_existed', function ($attribute, $value, $parameters, $validator) {
            return !!Voucher::where('code', $value)->count();
        });
    }

    protected function registerVoucherIsExpired()
    {
        Validator::extend('voucher_is_expired', function ($attribute, $value, $parameters, $validator) {
            $voucher = Voucher::where('code', $value)->first();
            if ($voucher) {
                $expiresAt = Carbon::createFromTimestamp($voucher->expires_at/1000)->endOfDay();
                return Carbon::now()->lt($expiresAt);
            }
            return true;
        });
    }

    protected function registerUniqueVoucher()
    {
        Validator::extend('unique_voucher', function ($attribute, $value, $parameters, $validator) {
            return !Bounty::where('type', Bounty::VOUCHER_TYPE)
                ->where('meta_data', $value)
                ->where('user_id', $parameters)
                ->count();
        });
    }

    protected function registerValidateOtpCode()
    {
        Validator::extend('is_invalid_otp_code', function ($attribute, $value, $parameters, $validator) {
            $user = User::find($parameters[0]);
            $googleAuthenticator = new PHPGangsta_GoogleAuthenticator();
            return $googleAuthenticator->verifyCode($user->two_factor_secret, $value, 0);
        });
    }

    protected function registerUniqueMetadataBounty()
    {
        Validator::extend('unique_metadata_bounty', function ($attribute, $value, $parameters, $validator) {
            return !Bounty::where('meta_data', $value)
                ->where('user_id', $parameters[0])
                ->where('type', $parameters[1])
                ->count();
        });
    }

    protected function registerUniqueCurrencyAddress()
    {
        Validator::extend('unique_currency_address', function ($attribute, $value, $parameters, $validator) {
            $isValidInUsersTable = !User::where("{$parameters[0]}_address", $value)->count();
            $isValidInHistory = !AddressHistory::where('address', $value)
                ->where('currency', $parameters[0])
                ->where('user_id', '!=', $parameters[1])
                ->count();

            return $isValidInUsersTable && $isValidInHistory;
        });
    }

    protected function isNotTempEmail()
    {
        Validator::extend('is_not_temp_email', function ($attribute, $value, $parameters, $validator) {
            return !is_disposable_email($value);
        });
    }
}
