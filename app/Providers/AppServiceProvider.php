<?php

namespace App\Providers;

use App\Http\Services\UserService;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        if (env('APP_ENV') !== 'production') {
            DB::enableQueryLog();
            DB::listen(function ($query) {
                info('SQL', [
                    'sql' => $query->sql,
                    'bindings' => $query->bindings,
                    'runtime' => $query->time
                ]);
            });
        }

        View::composer('*', function ($view) {
            $userLocale = app()->make(UserService::class)->getCurrentUserLocale();
            $view->with('userLocale', $userLocale);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
