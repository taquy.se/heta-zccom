<?php

namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\Access\AuthorizationException;

class ApiResponse
{
    public static function success($rawData = [], $httpCode = 200)
    {
        $meta = [];
        $data = $rawData;

        if ($data instanceof LengthAwarePaginator) {
            $rawData = $rawData->toArray();
            $data = $rawData['data'];
            $meta = $rawData;
            unset($meta['data']);
        }

        $meta = array_merge(['timestamp' => time()], $meta);
        return response()->json([
            'data' => $data,
            'meta' => $meta,
        ], $httpCode);
    }
}
