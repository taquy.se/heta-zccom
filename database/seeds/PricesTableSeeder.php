<?php

use App\Models\Price;
use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Price::truncate();

        $prices = [
            [
                'coin' => 'btc',
                'price' => '6517.51045647'
            ],
            [
                'coin' => 'eth',
                'price' => '220.69356689'
            ],
            [
                'coin' => 'usdt',
                'price' => '1.0043706943'
            ],
            [
                'coin' => 'heta',
                'price' => '0.0017'
            ]
        ];

        Price::insert($prices);
    }
}
