<?php

use Illuminate\Database\Seeder;
use App\Models\ColdWallet;
use App\Consts;

class ColdWalletsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ColdWallet::truncate();

        ColdWallet::create(['currency' => 'btc', 'address' => '13nvtkqX3mp1qBkYDjYSxS9zhDJMfgajnG']);
        ColdWallet::create(['currency' => 'eth', 'address' => '0x402128f6b0c6137ae9562971362df43404c8ad8d']);
        ColdWallet::create(['currency' => 'usdt', 'address' => '126VtDnVRzVmomEj4kuikvxtToEBKA12qS']);
    }
}
