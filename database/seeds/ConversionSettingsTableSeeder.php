<?php

use Illuminate\Database\Seeder;
use App\Models\ConversionSetting;
use Carbon\Carbon;

class ConversionSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ConversionSetting::truncate();
        $now = Carbon::now();
        $data = [
            [
                'currency' => 'btc',
                'min_token' => 1,
                'currency_precision' => 8,
                'token_precision' => 0,
                'min_currency' => '0.001',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'currency' => 'eth',
                'min_token' => 1,
                'currency_precision' => 8,
                'token_precision' => 0,
                'min_currency' => '0.01',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'currency' => 'usdt',
                'min_token' => 1,
                'currency_precision' => 5,
                'token_precision' => 0,
                'min_currency' => '10',
                'created_at' => $now,
                'updated_at' => $now
            ],
        ];
        ConversionSetting::insert($data);
    }
}
