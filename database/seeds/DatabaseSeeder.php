<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BountySettingsTableSeeder::class);
        $this->call(PricesTableSeeder::class);
        $this->call(SystemSettingsTableSeeder::class);
        $this->call(ConversionSettingsTableSeeder::class);
        $this->call(ColdWalletsTableSeeder::class);
        $this->call(BonusesTableSeeder::class);
    }
}
