<?php

use App\Models\Bounty;
use Illuminate\Database\Seeder;
use App\Models\BountySetting;

class UpdateBountySettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bugBounty = BountySetting::where('key', 'bug_report')->first();
        $bugBounty->value = 100;
        $bugBounty->save();
    }
}
