<?php

use Illuminate\Database\Seeder;
use App\Models\Voucher;
use Carbon\Carbon;

class VouchersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Voucher::truncate();
        Voucher::create([
            'expires_at' => Carbon::now()->addDays(30)->timestamp*1000,
            'token' => 150,
        ]);
        Voucher::create([
            'expires_at' => Carbon::now()->subDay()->timestamp*1000,
            'token' => 150,
        ]);
    }
}
