<?php

use App\Models\Bounty;
use Illuminate\Database\Seeder;
use App\Models\BountySetting;

class BountySettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BountySetting::truncate();

        $data = [
            [
                'key' => 'max_heta_tokens',
                'display_name' => 'Max Receivable Bounty & Airdrop Tokens',
                'value' => '10000',
                'display_order' => 0,
            ],
            [
                'key' => Bounty::REFERRAL_REGISTER_TYPE,
                'display_name' => 'Tokens Per Referral Register',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_REFERRAL_PROGRAM,
            ],
            [
                'key' => Bounty::REFERRAL_KYC_TYPE,
                'display_name' => 'Tokens Per Referral KYC',
                'value' => 100,
                'display_order' => \App\Consts::DISPLAY_ORDER_REFERRAL_PROGRAM,
            ],
            [
                'key' => Bounty::REFERRAL_DEPOSIT_TYPE,
                'display_name' => 'Tokens Per Referral Deposit',
                'value' => 150,
                'display_order' => \App\Consts::DISPLAY_ORDER_REFERRAL_PROGRAM,
            ],
            [
                'key' => 'facebook_share',
                'display_name' => 'Tokens Per Facebook Share',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'google_share',
                'display_name' => 'Tokens Per Google Share',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'twitter_share',
                'display_name' => 'Tokens Per Twitter Share',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'linkedin_share',
                'display_name' => 'Tokens Per LinkedIn Share',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'facebook_like',
                'display_name' => 'Tokens Per Facebook Like',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'medium_follow',
                'display_name' => 'Tokens Per Medium Follow',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            // [
            //     'key' => 'reddit_follow',
            //     'display_name' => 'Tokens Per Reddit Follow',
            //     'value' => 50,
            //     'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            // ],
            [
                'key' => 'bitcoin_talk_follow',
                'display_name' => 'Tokens Per BitcoinTalk Follow',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'steemit_follow',
                'display_name' => 'Tokens Per Steemit Follow',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'twitter_follow',
                'display_name' => 'Tokens Per Twitter Follow',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'linkedin_connection',
                'display_name' => 'Tokens Per LinkedIn Connection',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'telegram_join',
                'display_name' => 'Tokens Per Telegram Join',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'telegram_news_join',
                'display_name' => 'Tokens Per Telegram News Join',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'youtube_subscribe',
                'display_name' => 'Tokens Per Youtube Subscribe',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_SNS_LIKE_SHARE,
            ],
            [
                'key' => 'bitcoin_talk_post',
                'display_name' => 'Tokens Per BitcoinTalk Post',
                'value' => 100,
                'display_order' => \App\Consts::DISPLAY_ORDER_WRITING_FILMING_ABOUT_HETA,
            ],
            [
                'key' => 'steemit_post',
                'display_name' => 'Tokens Per Steemit Post',
                'value' => 100,
                'display_order' => \App\Consts::DISPLAY_ORDER_WRITING_FILMING_ABOUT_HETA,
            ],
            [
                'key' => 'reddit_post',
                'display_name' => 'Tokens Per Reddit Post',
                'value' => 100,
                'display_order' => \App\Consts::DISPLAY_ORDER_WRITING_FILMING_ABOUT_HETA,
            ],
            [
                'key' => 'medium_post',
                'display_name' => 'Tokens Per Medium Post',
                'value' => 100,
                'display_order' => \App\Consts::DISPLAY_ORDER_WRITING_FILMING_ABOUT_HETA,
            ],
            [
                'key' => 'linkedin_post',
                'display_name' => 'Tokens Per LinkedIn Post',
                'value' => 100,
                'display_order' => \App\Consts::DISPLAY_ORDER_WRITING_FILMING_ABOUT_HETA,
            ],
            [
                'key' => 'facebook_post',
                'display_name' => 'Tokens Per Facebook Post',
                'value' => 100,
                'display_order' => \App\Consts::DISPLAY_ORDER_WRITING_FILMING_ABOUT_HETA,
            ],
            [
                'key' => 'blog_post',
                'display_name' => 'Tokens Per Blog Post',
                'value' => 100,
                'display_order' => \App\Consts::DISPLAY_ORDER_WRITING_FILMING_ABOUT_HETA,
            ],
            [
                'key' => 'other_website_post',
                'display_name' => 'Tokens Per Post On Other Sites',
                'value' => 100,
                'display_order' => \App\Consts::DISPLAY_ORDER_WRITING_FILMING_ABOUT_HETA,
            ],
            [
                'key' => 'other_video_post',
                'display_name' => 'Tokens Per Video On Other Sites',
                'value' => 500,
                'display_order' => \App\Consts::DISPLAY_ORDER_WRITING_FILMING_ABOUT_HETA,
            ],
            [
                'key' => 'bug_report',
                'display_name' => 'Tokens Per Bug Report',
                'value' => 0,
                'display_order' => \App\Consts::DISPLAY_ORDER_BUG_BOUNTY,
            ],
            [
                'key' => 'register',
                'display_name' => 'Tokens For Registration',
                'value' => 50,
                'display_order' => \App\Consts::DISPLAY_ORDER_REGISTRATION,
            ],
            [
                'key' => 'kyc',
                'display_name' => 'Tokens For KYC',
                'value' => 100,
                'display_order' => \App\Consts::DISPLAY_ORDER_REGISTRATION,
            ],
            [
                'key' => 'deposit',
                'display_name' => 'Tokens For The First Deposit',
                'value' => 150,
                'display_order' => \App\Consts::DISPLAY_ORDER_REGISTRATION,
            ],
        ];
        BountySetting::insert($data);
    }
}
