<?php

use Illuminate\Database\Seeder;
use App\Models\Whitelist;

class WhitelistsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Whitelist::truncate();
        factory(Whitelist::class, 100)->create();
    }
}
