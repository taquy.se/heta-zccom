<?php

use Illuminate\Database\Seeder;
use App\Models\Bonus;

class BonusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bonus::truncate();
        $data = [
            [
                'timestamp' => '1541026800000',
                'percent' => 0.2
            ],
            [
                'timestamp' => '1542582000000',
                'percent' => 0.15
            ],
            [
                'timestamp' => '1544137200000',
                'percent' => 0.1
            ],
            [
                'timestamp' => '1545692400000',
                'percent' => 0.05
            ],
            [
                'timestamp' => '1547247600000',
                'percent' => 0
            ],
        ];
        Bonus::insert($data);
    }
}
