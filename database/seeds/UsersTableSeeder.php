<?php

use App\Models\Wallet;
use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        Wallet::truncate();

        User::create([
            'name' => 'Super User',
            'email' => 'admin@heta.org',
            'password' => Hash::make('123123'),
            'is_verified' => true,
            'role' => User::ADMIN_ROLE,
        ]);
        User::create([
            'name' => 'Test01',
            'email' => 'test01@heta.org',
            'password' => Hash::make('123123'),
            'is_verified' => true,
            'role' => User::USER_ROLE,
        ]);
        User::create([
            'name' => 'Test02',
            'email' => 'test02@heta.org',
            'password' => Hash::make('123123'),
            'is_verified' => true,
            'role' => User::USER_ROLE,
        ]);
        User::create([
            'name' => 'Test03',
            'email' => 'test03@heta.org',
            'password' => Hash::make('123123'),
            'is_verified' => true,
            'role' => User::USER_ROLE,
        ]);

        $this->createWallets();
    }

    private function createWallets()
    {
      $users = User::where('role',User::USER_ROLE)->get();

      foreach ($users as $user) {
          $user->wallets()->createMany([
              ['currency' => 'btc'],
              ['currency' => 'eth'],
              ['currency' => 'usdt'],
              ['currency' => 'heta'],
          ]);
      }
    }
}
