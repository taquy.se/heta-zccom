<?php

use Illuminate\Database\Seeder;
use App\Models\SystemSetting;
use App\Consts;
use Carbon\Carbon;

class SystemSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SystemSetting::truncate();

        $data = [
            [
                'key' => Consts::CONFIRMATION_BTC,
                'value' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => Consts::CONFIRMATION_ETH,
                'value' => 30,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => Consts::CONFIRMATION_USDT,
                'value' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => Consts::ICO_START_TIME,
                'value' => '1541026800000',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => Consts::ICO_END_TIME,
                'value' => '1548975599000',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => Consts::ICO_PRICE_HETA_TOKEN_PER_USD,
                'value' => 0.016,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => Consts::ICO_HARDCAP_BY_USD,
                'value' => 320000000,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => Consts::MIN_DEPOSIT_BTC,
                'value' => 0.0001,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => Consts::MIN_DEPOSIT_ETH,
                'value' => 0.01,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'key' => Consts::MIN_DEPOSIT_USDT,
                'value' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        SystemSetting::insert($data);
    }
}
