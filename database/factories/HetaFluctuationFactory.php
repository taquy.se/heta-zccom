<?php

use App\Utils;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\HetaFluctuation::class, function (Faker $faker) {
    $isConversion = $faker->boolean(50);
    $lastFlux = \App\Models\HetaFluctuation::latest()->first();
    return [
        'user_id' => App\Models\User::inRandomOrder()->first()->id,
        'ending_balance' => $faker->numberBetween(10, 100),
        'amount' => $faker->numberBetween(1, 10),
        'action_id' => $lastFlux ? $lastFlux->id + 1 : 1,
        'action_type' => $isConversion ? 'convert' : 'bounty',
        'object' => $faker->numberBetween(50, 500),
        'object_type' => $isConversion ? $faker->randomElement(['btc', 'eth', 'usdt']) : \App\Models\BountySetting::inRandomOrder()->first()->key,
        'created_at' => Utils::currentMilliseconds(),
        'updated_at' => Utils::currentMilliseconds(),
    ];
});
