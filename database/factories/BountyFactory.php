<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Bounty::class, function (Faker $faker) {
	$isVoucher = $this->faker->boolean(30);
	$bountySetting = $isVoucher ? App\Models\Voucher::inRandomOrder()->first() : \App\Models\BountySetting::inRandomOrder()->first();

    return [
        'user_id' => App\Models\User::inRandomOrder()->first()->id,
        'type' => $isVoucher ? App\Models\Bounty::VOUCHER_TYPE : $bountySetting->key,
        'meta_data' => $isVoucher ? $bountySetting->code : $faker->sentence,
        'heta_tokens' => $isVoucher ? $bountySetting->token : $bountySetting->value,
        'status' => App\Models\Bounty::STATUS_PENDING,
    ];
});
