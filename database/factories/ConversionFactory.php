<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Conversion::class, function (Faker $faker) {
    $confirmations = $faker->numberBetween(1, 10);
    $user = App\Models\User::inRandomOrder()->first();
    return [
        'user_id' => $user->id,
        'email' => $user->email,
        'currency' => $faker->randomElement(['btc', 'eth', 'usdt']),
        'currency_to_usd_price' => $faker->numberBetween(400, 8000),
        'token_to_currency_price' => $faker->randomFloat(8, 0, 1),
        'currency_amount' => $faker->numberBetween(1, 1000),
        'total_token_amount' => $faker->numberBetween(100, 10000),
        'bonus_percent' => $faker->randomFloat(2, 0, 1),
        'bonus_amount' => $faker->numberBetween(1, 200),
        'token_amount' => $faker->numberBetween(1, 1000),
        'created_at' => \App\Utils::currentMilliseconds(),
        'updated_at' => \App\Utils::currentMilliseconds(),
        'created_day_london' => $faker->dateTimeBetween($startDate = '-15 days', $endDate = 'now')->format('Y-m-d')
    ];
});
