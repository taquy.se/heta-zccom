<?php

use App\Utils;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Transaction::class, function (Faker $faker) {
    $confirmations = $faker->numberBetween(1, 10);
    $user = App\Models\User::inRandomOrder()->first();
    return [
        'user_id' => $user->id,
        'currency' => $faker->randomElement(['btc', 'eth', 'usdt']),
        'amount' => $faker->numberBetween(1, 10),
        'txid' => $faker->uuid,
        'to_address' => $faker->uuid,
        'confirmations' => $confirmations,
        'status' => $faker->randomElement(['waiting', 'confirming', 'succeeded', 'failed']),
        'type' => $faker->randomElement(['deposit', 'withdraw']),
        'email' => $user->email,
        'is_confirm' => $confirmations === 10,
        'created_at' => Utils::currentMilliseconds(),
        'updated_at' => Utils::currentMilliseconds(),
    ];
});
