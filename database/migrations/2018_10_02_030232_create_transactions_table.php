<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('currency', 20);
            $table->decimal('amount', 30, 10)->default(0);
            $table->string('txid', 150)->nullable();
            $table->string('to_address');
            $table->unsignedInteger('confirmations')->default(0);
            $table->unsignedBigInteger('user_id')->index();
            $table->string('email');
            $table->enum('type', ['deposit', 'withdraw']);
            $table->enum('status', ['waiting', 'confirming', 'succeeded', 'failed'])->default('waiting');
            $table->boolean('is_confirm')->default(false);
            $table->bigInteger('created_at');
            $table->bigInteger('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
