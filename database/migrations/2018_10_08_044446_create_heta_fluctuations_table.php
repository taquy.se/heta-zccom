<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHetaFluctuationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heta_fluctuations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedDecimal('ending_balance', 30, 10)->default(0);
            $table->decimal('amount', 30, 10)->default(0);
            $table->string('action_type');
            $table->unsignedInteger('action_id');
            $table->string('object')->nullable();
            $table->string('object_type');
            $table->bigInteger('created_at');
            $table->bigInteger('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heta_fluctuations');
    }
}
