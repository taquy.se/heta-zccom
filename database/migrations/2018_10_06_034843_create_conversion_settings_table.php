<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversionSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversion_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('currency', 20);
            $table->decimal('min_token', 30, 10);
            $table->decimal('min_currency', 30, 10);
            $table->integer('currency_precision');
            $table->integer('token_precision');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversion_settings');
    }
}
