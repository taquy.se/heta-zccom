<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id');
            $table->string('email');
            $table->string('currency', 20);
            $table->decimal('currency_to_usd_price', 30, 10)->default(0);
            $table->decimal('token_to_currency_price', 30, 10)->default(0);
            $table->decimal('currency_amount', 30, 10)->default(0);
            $table->decimal('token_amount', 30, 10)->default(0);
            $table->decimal('total_token_amount', 30, 10)->default(0);
            $table->float('bonus_percent')->default(0);
            $table->decimal('bonus_amount', 30, 10)->default(0);
            $table->bigInteger('created_at');
            $table->date('created_day_london');
            $table->bigInteger('updated_at');

            $table->index('created_at');
            $table->index(['user_id', 'created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversions');
    }
}
