<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEthAddressHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eth_address_histories', function (Blueprint $table) {
            $table->string('currency', 20)->default('eth')->after('eth_address');
            $table->renameColumn('eth_address', 'address');
        });

        Schema::rename('eth_address_histories', 'address_histories');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eth_address_histories', function (Blueprint $table) {
            //
        });
    }
}
