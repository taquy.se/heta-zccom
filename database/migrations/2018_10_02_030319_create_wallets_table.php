<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedDecimal('balance', 30, 10)->default(0);
            $table->unsignedDecimal('available_balance', 30, 10)->default(0);
            $table->unsignedDecimal('bounty_amount', 30, 10)->default(0);
            $table->boolean('approve_status')->default(false);
            $table->string('address')->nullable();
            $table->string('currency', 20);
            $table->timestamps();

            $table->unique(['currency', 'address']);
            $table->unique(['user_id', 'currency']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}
