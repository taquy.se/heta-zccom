sudo yum install -y expect

#install php
sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
sudo yum -y install php71w php71w-opcache php71w-xml php71w-mcrypt php71w-gd php71w-devel php71w-mysqlnd php71w-intl php71w-mbstring php71w-bcmath

#install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php --install-dir=/usr/local/bin/ --filename=composer

php -r "unlink('composer-setup.php');"

#install nodejs
curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
sudo yum -y install nodejs

# install git
sudo yum -y install git

# install jenkins
sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
sudo yum install jenkins

#install java for jenkins
sudo yum install java

#start jenkin service
sudo service jenkins start/stop/restart

#auto boot
sudo chkconfig jenkins on

