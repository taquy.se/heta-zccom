email=$1
date=`date`
time=`date +%s`

source_folder="/heta_ico_deploy/heta_ico"
commit=`cd $source_folder && git log -n 1 | sed ':a;N;$!ba;s#\n#<br />#g;s#^<br />##g'`

subject="Heta ICO deployment $date ($time)"
content="All servers are updated to:<br/>$commit"
cd $source_folder && php artisan email:send $email "$subject" "$content"