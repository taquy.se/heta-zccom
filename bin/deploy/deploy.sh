h=$1
deploy=$2
ssh -o StrictHostKeyChecking=No -i /heta_ico_deploy/hetachainsgp.pem ec2-user@$h "mkdir -p /var/www/heta_ico"
if [ $deploy -eq "1" ]; then
    rsync -avhzL --delete \
            --no-perms --no-owner --no-group \
            --exclude .git \
            --exclude .idea \
            --exclude .env \
            --exclude laravel-echo-server.json \
            --exclude bootstrap/cache \
            --exclude storage/logs \
            --exclude storage/framework \
            --exclude storage/app \
            --exclude public/storage \
            /heta_ico_deploy/heta_ico/ -e "ssh -o StrictHostKeyChecking=No -i /heta_ico_deploy/hetachainsgp.pem" ec2-user@$h:/var/www/heta_ico/
    exit;
fi
rsync -avhzL --delete \
            --no-perms --no-owner --no-group \
            --exclude .git \
            --exclude .idea \
            --exclude .env \
            --exclude laravel-echo-server.json \
            --exclude bootstrap/cache \
            --exclude storage/logs \
            --exclude storage/framework \
            --exclude storage/app \
            --exclude public/storage \
	    --dry-run \
            /heta_ico_deploy/heta_ico/ -e "ssh -o StrictHostKeyChecking=No -i /heta_ico_deploy/hetachainsgp.pem" ec2-user@$h:/var/www/heta_ico/