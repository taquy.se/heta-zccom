#install apache
sudo yum install -y httpd
sudo cp ./bin/heta_ico.conf /etc/httpd/conf.d/

#install php
sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
sudo yum -y install php71w php71w-opcache php71w-xml php71w-mcrypt php71w-gd php71w-devel php71w-mysqlnd php71w-intl php71w-mbstring php71w-bcmath

#make storage and cache writable
sudo mkdir -p /var/www/heta_ico/bootstrap/cache
sudo mkdir -p /var/www/heta_ico/storage/framework/cache
sudo mkdir -p /var/www/heta_ico/storage/framework/sessions
sudo mkdir -p /var/www/heta_ico/storage/framework/views
sudo mkdir -p /var/www/heta_ico/storage/app/public
sudo mkdir -p /var/www/heta_ico/storage/logs
sudo mkdir -p /var/www/heta_ico/public/
sudo chown -R apache:apache /var/www/heta_ico/bootstrap/cache
sudo chown -R apache:apache /var/www/heta_ico/storage/framework
sudo chown -R apache:apache /var/www/heta_ico/storage/app
sudo chown -R apache:apache /var/www/heta_ico/storage/logs
sudo setfacl -d -m g:apache:rwx /var/www/heta_ico/storage/logs
sudo ln -s /var/www/heta_ico/storage/app/public /var/www/heta_ico/public/storage

sudo chmod -R +w /var/www/heta_ico/storage
sudo chmod -R +w /var/www/heta_ico/bootstrap/cache

sudo restorecon -R /var/www
sudo chcon -t httpd_sys_rw_content_t -R /var/www/heta_ico/storage
sudo chcon -t httpd_sys_rw_content_t -R /var/www/heta_ico/bootstrap/cache

sudo systemctl start httpd.service
sudo systemctl enable httpd.service

sudo yum install -y supervisor
sudo cp heta_ico.ini /etc/supervisor.d/
sudo touch /var/www/heta_ico/storage/logs/worker.log
sudo chmod 777 /var/www/heta_ico/storage/logs/worker.log
sudo systemctl start supervisord
sudo systemctl enable supervisord

#make apache be able to connect network
sudo setsebool -P httpd_can_network_connect 1