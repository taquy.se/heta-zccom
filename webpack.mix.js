const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const OUTPUT_DIRECTORY = 'public/build/';
const OUTPUT_JS_DIRECTORY = OUTPUT_DIRECTORY + 'js/';
const OUTPUT_CSS_DIRECTORY = OUTPUT_DIRECTORY + 'css/';

mix
  .js('resources/assets/js/home/app.js', OUTPUT_JS_DIRECTORY)
  .js('resources/assets/js/mobile/app.js', OUTPUT_JS_DIRECTORY  + 'mobile')
  .js('resources/assets/js/admin/admin.js', OUTPUT_JS_DIRECTORY)
  .js('resources/assets/js/home/background.js', OUTPUT_JS_DIRECTORY)
  .copy('resources/assets/js/common/socket.io.js', 'public/js')
  .sass('resources/assets/sass/app.scss', OUTPUT_CSS_DIRECTORY)
  .sass('resources/assets/sass/mobile/app.scss', OUTPUT_CSS_DIRECTORY + 'mobile')
  .version();
