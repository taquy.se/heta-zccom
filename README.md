# Heta ICO Website

### Pre-requisites

  - PHP >= 7.0.0
  - MySQL
  - Composer
  - Node JS
  - Docker
  - make

### Using docker
- We created a Dockerfile to help you set up project in a fastest way and the environment is consistent between different computers.
- Start the docker containers:
```sh
$ make docker-start
```

- First time install
```sh
$ make docker-init
```

- Compile assets
```sh
$ make watch
```

- Test it out in localhost:8003 using this credential: test01@heta.org/123123

### First time setup
  - For any reason that you cannot use Docker, you can try the following usual way
  - copy .env file from .env.example and then change database configuration
```sh
$ cp .env.example .env
```
  - install composer modules. You'll need to run this command again if there's any update in the file composer.json
```sh
$ composer install
```
- Generate application key
```sh
$ php artisan key:generate
```
- Init database
```sh
$ php artisan migrate
$ php artisan db:seed
```
- Link storage folder
```sh
$ php artisan storage:link
```
- Install Laravel Passport
```sh
$ php artisan passport:install
```
### Compiling assets
We use Laravel Mix and webpack to manage and version our assets (javascript, css). Here's how to to do it:
  - Install node modules. You only need to run this command again if there's any update in the file package.json
```sh
$ npm install
```
  - Compile assets
```sh
$ npm run dev
```
  - If you dont want to run the above command every time you update a javscript or css file, you can use the following command to watch and compile automatically
```sh
$ npm run watch
```

### Test it out
- Run web server
```sh
$ php artisan serve
```

### Coding convention

- Indentation: 2 spaces for html/js and 4 spaces for php/css
- No trailing space

- JS: `npm test`
- PHP:
  - `vendor/bin/phpcs --standard=phpcs.xml`
  - `vendor/bin/phpmd app text phpmd.xml`

### Shortcut by Makefile
- If you're familiar with Make file, you can use the following commands as shortcuts to perform various actions during the development process
```shell
make init-db-full     # Reset databse
make docker-restart   # Restart docker and database
make deploy-dev       # Deploy to test server
make log              # Tail Laravel log
make test-js          # Check standard *.js
make test-php         # Check standard *.php
make watch            # Watch assets (*.js, *css)
make autoload         # Reload *.php
make cache            # Clear cache
make route            # List routes
```

### How to test your API with Postman
In order to request an protected restful API, you need an OAuth 2 access token. Here's how:
- First, create a Password Grant Client
```
$ php artisan passport:client --password
```
- Then you can generate a Personal Access Token for any user by making a Post request like this: https://laravel.com/docs/5.5/passport#requesting-tokens

