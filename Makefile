ifndef env
env:=dev
endif


docker-start:
	docker-compose up -d

docker-restart:
	docker-compose down
	make docker-start
	make docker-init-db-full
	make docker-link-storage

docker-connect:
	docker exec -it heta_ico bash

init-app:
	cp .env.example .env
	cp laravel-echo-server.json.example laravel-echo-server.json
	composer install
	rm -rf node_modules
	rm -f package-lock.json
	npm install
	php artisan key:generate
	php artisan migrate:fresh --seed
	php artisan passport:install

docker-init:
	docker exec -it heta_ico make init-app
	docker exec -it heta_ico chmod -R 777  storage

init-db-full:
	make autoload
	php artisan migrate:fresh --seed

docker-init-db-full:
	docker exec -it heta_ico make init-db-full

docker-link-storage:
	docker exec -it heta_ico php artisan storage:link

log:
	tail -f "./storage/logs/$(shell ls ./storage/logs/ | sed 's/\([0-9]\+\).*/\1/g' | sort -n | tail -1)"

deploy:
	ssh $(u)@$(h) "mkdir -p $(dir)"
	rsync -avhzL --delete \
				--no-perms --no-owner --no-group \
				--exclude .git \
				--exclude .idea \
				--exclude echo.json \
				--exclude .env \
				--exclude laravel-echo-server.json \
				--exclude node_modules \
				--exclude /vendor \
				--exclude bootstrap/cache \
				--exclude storage/logs \
				--exclude storage/framework \
				--exclude storage/app \
				--exclude public/storage \
				. $(u)@$(h):$(dir)/

deploy-dev:
	make deploy h=192.168.1.20$(n) dir=/var/www/heta_ico
	ssh $(u)@192.168.1.20$(n) "cd /var/www/heta_ico && make cache"

deploy-prod:
	make deploy u=ec2-user h=52.76.4.106 dir=/home/ec2-user/heta_ico_web
	ssh ec2-user@52.76.4.106 "cd /home/ec2-user/heta_ico && make deploy-prod-refer"

deploy-prod-refer:
	make deploy u=ec2-user h=10.69.0.252 dir=/var/www/heta_ico_web
	ssh ec2-user@10.69.0.252 "cd /var/www/heta_ico && make cache"

watch:
	docker exec -it heta_ico npm run watch

autoload:
	docker exec -it heta_ico composer dump-autoload

cache:
	php artisan cache:clear && php artisan view:clear && npm cache clean --force

docker-cache:
	docker exec -it heta_ico make cache

init-modules:
	docker exec -it heta_ico npm install

route-list:
	docker exec -it heta_ico php artisan route:list

refresh-db:
	docker exec -it heta_ico composer dump-autoload
	docker exec -it heta_ico php artisan migrate:fresh --seed
	docker exec -it heta_ico php artisan passport:install

work:
	docker exec -it heta_ico php artisan queue:work

seed:
	docker exec -it heta_ico php artisan db:seed --class=$(c)
migrate:
	docker exec -it heta_ico php artisan migrate
php-artisan:
	# Ex: make php-artisan cmd='db:seed --class=RolePermissionsTableSeeder'
	docker exec -it heta_ico php artisan $(cmd)
create-table:
	# Ex: make create-alter n=create_users_table t=users
	docker exec -it heta_ico php artisan make:migration $(n) --create=$(t)
create-model:
	# Ex: make create-model n=Test
	# Result: app/Models/Test.php
	#         database/migrations/2018_01_05_102531_create_tests_table.php
	docker exec -it heta_ico php artisan make:model Models/$(n) -m
create-alter:
	# Ex: make create-alter n=add_votes_to_users_table t=users
	docker exec -it heta_ico php artisan make:migration $(n) --table=$(t)
docker-queue:
	docker exec -it heta_ico php artisan queue:work
localization:
	docker exec -it heta_ico php artisan localization:sort
