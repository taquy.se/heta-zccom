<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="{{ $userLocale }}" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Heta Token</title>
    </head>
    <body style="-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;Margin:0;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important">
      <table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0"
       style="font-size:14px;font-family:Microsoft Yahei,Arial,Helvetica,sans-serif;padding:0;margin:0;color:#333;background-image:url({{ url('/images/mail_footer.jpg') }});background-color:#f7f7f7;background-repeat:repeat-x;background-position:bottom left">
        <tbody>
          <tr>
            <td>
              <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                <tr>
                  <td align="center" valign="middle" style="padding:33px 0">
                    <a href="https://www.heta.org/" target="_blank" style="display: table; text-decoration: none;">
                      <img src="{{ url('/images/logo-HETA2.png') }}" height="90" alt="heta logo" style="border:0">
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div style="padding:0 30px;background:#fff">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                        <tr>
                          <td style="border-bottom:1px solid #e6e6e6;font-size:18px;padding:20px 0">
                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                              <tbody>
                              <tr>
                                <td>Confirm Your Registration</td>
                                <td>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="font-size:14px;line-height:30px;padding:20px 0;color:#666">Welcome to HetaChain!<br>Click the
                            button below to complete verification:
                          </td>
                        </tr>
                        <tr>
                          <td style="padding:5px 0"><a href="{{ $url }}"
                                    style="padding:10px 28px;background: linear-gradient(#c35731, #f38c41);color:#fff;text-decoration:none" target="_blank">Verify
                              Email</a></td>
                        </tr>
                        <tr>
                          <td style="font-size:14px;line-height:26px;padding:20px 0 0 0;color:#666">If you can't confirm by
                            clicking the button above, please copy the address below to the browser address bar to
                            confirm.<br><span style="text-decoration:underline"><a
                                      href="{{ $url }}"
                                      target="_blank">{{ $url }}</a></span>
                          </td>
                        </tr>
                        <tr>

                          <td style="font-size:12px;line-height:25px;padding:20px 0 10px 0;color:#666;font-weight:bolder">
                            <span style="font-size:14px;color:red">5 Security Tips:</span><br>
                            * DO NOT give your password to anyone!<br>
                            * DO NOT call any phone number for someone claiming to be HetaChain  Support!<br>
                            * DO NOT send any money to anyone claiming to be a member of HetaChain!<br>
                            * Enable Google Two Factor Authentication!<br>
                            * Make sure you are visiting "<a href="http://www.heta.org" target="_blank">www.heta.org</a>"!<br>
                          </td>
                          <td>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding:20px 0 0 0;line-height:26px;color:#666">If this activity is not your own operation,
                            please contact us immediately.
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <!-- <a style="color:#e9b434" href="https://support.Heta .com/hc/en-us/" target="_blank">https://support.Heta .com/
                              <wbr>
                              hc/en-us/</a> -->
                            </td>
                        </tr>
                        <tr>
                          <td style="padding:30px 0 15px 0;font-size:12px;color:#999;line-height:20px">HetaChain Team<br>Automated
                            message. Please do not reply.
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td align="center" style="font-size:12px;color:#999;padding:20px 0">© 2018 Heta.org All Rights Reserved<br>URL&nbsp;<a
                            style="color:#999;text-decoration:none" href="https://www.heta.com/" target="_blank">www.heta.org</a>&nbsp;&nbsp;E-mail<a
                            href="mailto:support@heta.org" style="color:#999;text-decoration:none" target="_blank">
                      <wbr>
                      support@heta.org</a></td>
                </tr>
                </tbody>
              </table>
            </td>
          </tr>
          </tbody>
        </table>
    </body>
</html>
