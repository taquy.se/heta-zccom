<!DOCTYPE html>
<html lang="{{ $userLocale }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Heta Platform</title>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <title>{{ config('app.name', 'HETACHAIN') }}</title>
  <!-- Styles -->
  <link href="{{ mix('/build/css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div class="error container">
        <div class="d-flex justify-content-center">
            <img class="img-fluid" src="/images/icon/icc-logo-yellow.svg" alt="logo">
        </div>
        <div class="text-center">
            <h1 class="title pb-4 pt-5">We&rsquo;ll be back soon!</h1>
            <div class="message text-center mx-auto">
                <p>We are currently performing a scheduled maintenance.<br/>Sorry for any inconvenience.
                </p>
            </div>
        </div>
    </div>
</body>

</html>
