<!DOCTYPE html>
<html lang="{{ $userLocale }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="base-url" content="{{ url('/admin') }}">

  @if (Auth::check() && Auth::user()->isAdmin())
      <meta name="authenticated" content="1">
  @endif

  <script src="{{ mix('/js/socket.io.js') }}"></script>

  <title>{{ config('app.name', 'ADMIN HETACHAIN') }}</title>
  <!-- Styles -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
</head>
<body>
  <div id="app"></div>
  <script>
    // this url is used (in bootstrap.js) to connect to echo server
    var ECHO_URL = "{{ env('ECHO_URL') }}";
    var SUM_SUB_HOST = "{{ env('SUM_SUB_HOST') }}";
  </script>
  <script src="{{ mix('/build/js/admin.js') }}"></script>
</body>
</html>
