<!DOCTYPE html>
<html lang="{{ $userLocale }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:title" content="HETACHAIN - THE BIGGEST BLOCKCHAIN 3.0 NETWORK" />
  <meta name="og:description" content="Hetachain is a 3rd Generation Blockchain Technology invested by Relam Investments LLC, an international investment company headquartered in Dubai. The Hetachain ecosystem consists of Hetacoin, Heta Wallets, smart contracts, Heta App Store and Blockchain feature-enablers that enhance its applicability and make it the most revolutionary technology in its class." />
  <meta property="og:image" itemprop="thumbnailUrl" content="https://heta.org/assets/images/rectangle-2@2x.png" />
  <link rel="canonical" href="https://dashboard.heta.org" />
  <meta property="og:url" content="https://dashboard.heta.org" />

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta name="base-url" content="{{ url('/') }}">
  @if (Auth::check() && !Auth::user()->isAdmin())
      <meta name="authenticated" content="1">
  @endif

  <script src="{{ mix('/js/socket.io.js') }}"></script>

  <title>{{ config('app.name', 'HetaChain Platform') }}</title>
  <!-- Styles -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
  <link href="{{ mix('/build/css/app.css') }}" rel="stylesheet">
  <script src="https://test-api.sumsub.com/idensic/static/idensic.js" async></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
  <script src="https://use.fontawesome.com/04dc6ccda2.js"></script>
</head>
<body>
  <div id="app"></div>
  <script>
    // this url is used (in bootstrap.js) to connect to echo server
    var ECHO_URL = "{{ env('ECHO_URL') }}";
    var ETH_EXPLORER_URL = "{{ env('ETH_EXPLORER_URL') }}";
    var BTC_EXPLORER_URL = "{{ env('BTC_EXPLORER_URL') }}";
    var USDT_EXPLORER_URL = "{{ env('USDT_EXPLORER_URL') }}";
  </script>
  <script src="{{ mix('/build/js/app.js') }}"></script>
</body>
</html>
