<!DOCTYPE html>
<html lang="{{ $userLocale }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- CSRF Token -->

    <title>{{ config('app.name', 'HETACHAIN') }}</title>
    <!-- Styles -->
    <link href="{{ mix('/build/css/app.css') }}" rel="stylesheet">

    <style lang="css">
        .guest_page_form_header {
            margin-top: 0px!important;
        }

        .guest_page_form_header .sub_title {
            margin-top: 10px !important;
        }

        .guest_page_container.success .form_actions {
            margin-top: 30px;
        }

        .guest_page_container.error .form_actions {
            margin-top: 60px;
        }
        .sub_title {
            font-size: 20px!important;
            font-weight: bold!important;
            font-style: normal!important;
            font-stretch: normal!important;
            line-height: normal!important;
            letter-spacing: normal!important;
            color: #ffffff!important;
        }
        .guest_page_form .form-container {
            width: 420px !important;
        }
    </style>
</head>
<body>
<div id="app" class="guest_page">
    <div class="bg-wrapper">
        <background></background>
    </div>
    <div class="guest_page_container error">
        <div class="logo_container">
            <a href="{{ url('/login') }}">
                <img src="/images/logo-HETA2.png" alt="HETACHAIN" class="img">
            </a>
            <div class="guest_page_form">
              <template>
                <div class="form-container">
                    <div class="guest_page_form_header">
                      <div class="sub_title">
                        <p>@{{ $t('auth.reset_password.error_token') }}</p>
                      </div>
                    </div>
                    <div class="guest_page_form_body">
                        <div class="form_actions">
                            <a href="{{ url('/login') }}">
                                <button class="button-primary">@{{ $t('auth.btn.login') }}</button>
                            </a>
                        </div>
                    </div>
                </div>
              </template>
            </div>
        </div>
    </div>
    <guest-page-footer></guest-page-footer>
</div>
<script src="{{ mix('/build/js/background.js') }}"></script>
</body>
</html>
