import Layout from '../pages/Layout/Layout.vue';
import GuestLayout from '../pages/Layout/GuestLayout.vue';
import Login from '../pages/Auth/Login.vue';
import Register from '../pages/Auth/Register.vue';
import Terms from '../pages/Auth/Terms.vue';
import ForgotPassword from '../pages/Auth/ForgotPassword.vue';
import ResetPasswordForm from '../pages/Auth/ResetPasswordForm.vue';
import Dashboard from '../pages/Dashboard.vue';
import Kyc from '../pages/Kyc.vue';
import Whitelist from '../pages/Whitelist.vue';
import ProfileSetting from '../pages/ProfileSetting';
import BountyApp from '../pages/bounty_airdrop/BountyAirdropApp.vue';
import Transactions from '../pages/Transactions.vue';
import Faq from '../pages/Faq.vue';
import Balance from '../pages/Balance.vue';
import Deposit from '../pages/Deposit.vue';
import Setting2FA from '../pages/Setting2FA.vue';
import ChangePassword from '../pages/ChangePassword.vue';
import Guide from '../pages/Guide';
import TokenHistory from '../pages/TokenHistory';
import EmailVerify from '../pages/EmailVerify';

const routes = [
  {
    path: '/',
    component: GuestLayout,
    redirect: '/dashboard',
    children: [
      {
        path: '/login',
        component: Login,
        name: 'Login'
      },
      {
        path: '/register',
        component: Register,
        name: 'Register'
      },
      {
        path: '/reset-password',
        component: ForgotPassword,
        name: 'Forgot Password'
      },
      {
        path: '/terms',
        component: Terms,
        name: 'Terms'
      },
      {
        path: '/whitelist/create',
        component: Whitelist,
        name: 'Whitelist'
      },
      {
        path: '/guide',
        component: Guide,
        name: 'Guide'
      },
      {
        path: '/password/reset/:token',
        component: ResetPasswordForm,
        name: 'Reset Password'
      },
      {
        path: '/faq',
        name: 'FAQ',
        component: Faq
      },
      {
        path: '/email/verify/:token',
        name: 'EmailVerify',
        component: EmailVerify
      },
    ]
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard
      },
      {
        path: '/bounty',
        name: 'Bounty & Airdrop',
        component: BountyApp
      },
      {
        path: '/users/kyc',
        name: 'Kyc',
        component: Kyc
      },
      {
        path: '/users/profileSetting',
        name: 'Profile Setting',
        component: ProfileSetting
      },
      {
        path: '/users/profileSetting/change-password',
        name: 'ChangePassword',
        component: ChangePassword
      },
      {
        path: '/wallet/transactions',
        name: 'Deposit History',
        component: Transactions
      },
      {
        path: '/balances',
        name: 'Balance',
        component: Balance
      },
      {
        path: '/deposit',
        name: 'Deposit',
        component: Deposit
      },
      {
        path: '/users/profileSetting/2FA',
        name: 'Setting2FA',
        component: Setting2FA,
      },
      {
        path: '/token-history',
        name: 'Token History',
        component: TokenHistory,
      },
    ]
  },
  { path: '*', redirect: '/dashboard' }
]

export default routes;
