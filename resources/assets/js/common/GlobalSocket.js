import Vue from 'vue';
import rf from '../common/requests/RequestFactory';

export default class GlobalSocket {

  constructor () {

    //public channels
    this.listenForPricesChannel();

    const isAuthenticated = !!document.head.querySelector('meta[name="authenticated"]');
    if (isAuthenticated) {
      //user private channels
      window.app.$store.dispatch('getCurrentUser').then(res => {
        const userId = res.id;
        this.listenForUserPrivateChannel(userId);
        window.app.$store.dispatch('getWallets');
      })
    }

    Vue.mixin({
      mounted () {
        if (this.getSocketEventHandlers) {
          window._.forIn(this.getSocketEventHandlers(), (handler, eventName) => {
            this.$on(eventName, handler);
          });
        }
      },
      beforeDestroy () {
        if (this.getSocketEventHandlers) {
          window._.forIn(this.getSocketEventHandlers(), (handler, eventName) => {
            this.$off(eventName, handler);
          });
        }
      }
    });
  }

  listenForUserPrivateChannel (userId) {
    window.Echo.private('App.User.' + userId)
      .listen('BalanceUpdated', (balance) => {
        console.log('BalanceUpdated');
        window.app.$store.commit('updateWallet', balance.data);
        window.app.$broadcast('BalanceUpdated', balance.data);
      })
      .listen('TransactionUpdated', (transaction) => {
        window.app.$broadcast('TransactionUpdated', transaction.data);
      })
      .listen('ConversionCreated', (conversion) => {
        window.app.$broadcast('ConversionCreated', conversion.data);
      })
      .listen('BountyUpdated', (transaction) => {
        window.app.$broadcast('BountyUpdated', transaction.data);
      })
      .listen('HetaBalanceFluctuated', (fluctuation) => {
        window.app.$broadcast('HetaBalanceFluctuated', fluctuation.data);
      })
      .listen('UserUpdatedEvent', (user) => {
        window.app.$store.commit('updateUser', user.data);
      })
      .listen('UserSessionUpdatedEvent', (data) => {
        window.app.$store.dispatch('getCurrentUser');
      });
  }

  listenForPricesChannel () {
    window.Echo.channel('App.Prices')
      .listen('PricesUpdated', (newPrices) => {
        window.app.$broadcast('PricesUpdated', newPrices.data);
        window.app.$store.commit('updatePrice', newPrices.data);
      });
  }
}
