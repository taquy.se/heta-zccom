import NotImplementedException from './exceptions/NotImplementedException';

export default class BaseRequest {
  getEndpoint() {
    throw new NotImplementedException('getEndpoint must be implemented');
  }

  get(id, params = {}) {
    const url = `${this.getEndpoint()}/${id}`;
    return this._get(url, params);
  }

  getAll(params = {}) {
    return this._get(this.getEndpoint(), params);
  }

  update(id, params = {}) {
    const url = `${this.getEndpoint()}/${id}`;
    return this.put(url, params);
  }

  delete(id, params = {}) {
    const url = `${this.getEndpoint()}/${id}`;
    return this._delete(url, params);
  }

  create(params = {}) {
    const url = this.getEndpoint();
    return this.post(url, params);
  }

  _get(url, params = {}) {
    return new Promise((resolve, reject) => {
      window.axios
        .get(url, {
          params: params
        })
        .then((response) => {
          this._responseHandler(resolve, response);
        })
        .catch((error) => {
          this._errorHandler(reject, error);
        });
    });
  }

  put(url, data = {}) {
    return new Promise((resolve, reject) => {
      window.axios
        .put(url, data)
        .then((response) => {
          this._responseHandler(resolve, response);
        })
        .catch((error) => {
          this._errorHandler(reject, error);
        });
    });
  }

  post(url, data = {}) {
    return new Promise((resolve, reject) => {
      window.axios
        .post(url, data)
        .then((response) => {
          this._responseHandler(resolve, response);
        })
        .catch((error) => {
          this._errorHandler(reject, error);
        });
    });
  }

  updateWithFile(url, data = {}) {
    const headers = {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    };
    return new Promise((resolve, reject) => {
      window.axios
        .post(url, data, headers)
        .then((response) => {
          this._responseHandler(resolve, response);
        })
        .catch((error) => {
          this._errorHandler(reject, error);
        });
    });
  }

  _delete(url, params = {}) {
    return new Promise((resolve, reject) => {
      window.axios
        .delete(url, { params: params } )
        .then((response) => {
          this._responseHandler(resolve, response);
        })
        .catch((error) => {
          this._errorHandler(reject, error);
        });
    });
  }

  _responseHandler(resolve, res) {
    return resolve(res.data);
  }

  _errorHandler(reject, error) {
    const response = error.response;
    if (response.status === 401) { // Unauthorized (session timeout)
      window.app.$broadcast('Unauthorized');
    }
    if (response.status === 503 || response.status === 419) { // maintenance
      window.location.reload();
    }
    if (response.status === 400 && response.data.message && window.app) { // maintenance
      window.app.$toasted.show(response.data.message, {
        theme: 'bubble',
        position: 'bottom-right',
        duration : 2000
      });
    }
    return reject(response.data);
  }

}
