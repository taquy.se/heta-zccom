import Vue from 'vue';
import Vuex from 'vuex';
import moment from 'moment';

import rf from '../requests/RequestFactory';;

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    user: {},
    key2FA: '',
    prices: {},
    wallets: [],
    bonuses: [],
    currentBonus: null,
    systemSettings: {},
    conversionSettings: [],
    supportedLocales: ['en', 'vi', 'jp', 'fr'],
  },
  getters: {
    isCrowdsaling ({ systemSettings }) {
      const startAt = parseInt(systemSettings.ico_start_time || 0);
      const endAt = parseInt(systemSettings.ico_end_time || 0);
      const current = moment().valueOf();
      return (current > startAt) && (current < endAt);
    },
    expectedConfirmation: ({ systemSettings }) => (currency) => {
      return systemSettings[`confirmation_${currency}`] || 0;
    },
    minDeposit: ({ systemSettings }) => (currency) => {
      return systemSettings[`min_deposit_${currency}`] || 0;
    },
    hetaTokensWallet ({ wallets }) {
      return _.find(wallets, wallet => {
        return wallet.currency === 'heta';
      }) || {};
    },
    getPrecision: ({ conversionSettings }) => (currency) => {
      currency = _.toLower(currency);
      if (currency === 'heta') {
        const conversionSetting = _.head(conversionSettings) || {};
        return conversionSetting.token_precision || 0;
      }

      if (currency === 'usd') {
        currency = 'usdt';
      }

      const conversionSetting = conversionSettings.find((item) => item.currency === currency) || {};
      return conversionSetting.currency_precision || 8;
    }
  },
  mutations: {
    updateUser (state, data) {
      state.user = data;
    },
    update2FaSecret (state, data) {
      state.key2FA = data;
    },
    updateWallets (state, data) {
      state.wallets = data;
    },
    updateWallet (state, data) {
      const foundIndex = state.wallets.findIndex(wallet => wallet.id == data.id);
      if (foundIndex !== -1) {
        state.wallets.splice(foundIndex, 1, data);
      }
    },
    updateSystemSettings (state, data) {
      state.systemSettings = data;
    },
    updateConversionSettings (state, data) {
      state.conversionSettings = data;
    },
    updatePrice (state, data) {
      state.prices = Object.assign({}, state.prices, data);
    },
    updateBonuses (state, data) {
      const current = moment().format('x');
      const bonus = data.find(item => item.timestamp > current);
      if (bonus) {
        state.currentBonus = Object.assign({}, bonus);
        bonus.isActive = true;
      }
      state.bonuses = [...data];
    },
  },
  actions: {
    getCurrentUser ({ commit }) {
      return new Promise((resolve) => {
        rf.getRequest('UserRequest').getCurrentUser().then((res) => {
          commit('updateUser', res.data);
          resolve(res.data);
        });
      });
    },
    getWallets({ commit }) {
      return new Promise((resolve) => {
        rf.getRequest('WalletRequest').getAll().then((res) => {
          commit('updateWallets', res.data);
          resolve(res.data);
        });
      });
    },
    fetchSystemSettings ({ commit }) {
      return new Promise((resolve) => {
        rf.getRequest('SystemSettingRequest').getAll().then((res) => {
          commit('updateSystemSettings', res.data);
          resolve(res.data);
        });
      });
    },
    fetchDepositAddress({ commit, state, dispatch }, coin) {
      const wallet = state.wallets.find(wallet => wallet.currency === coin);

      if (wallet && wallet.address) {
        return new Promise((resolve) => {
          resolve(wallet);
        });
      }

      return new Promise((resolve) => {
        rf.getRequest('WalletRequest').generateDepositAddress(coin).then((res) => {
          if (state.wallets.length) {
            commit('updateWallet', res.data);
          } else {
            dispatch('getWallets');
          }
          resolve(res.data);
        });
      });
    },
    fetchConversionSettings ({ commit }) {
      return new Promise((resolve) => {
        rf.getRequest('ConversionRequest').getSettings().then((res) => {
          commit('updateConversionSettings', res.data);
          resolve(res.data);
        });
      });
    },
    fetchPrices ({ commit, state }) {
      if (Object.keys(state.prices).length !== 0) {
        return new Promise((resolve) => {
          resolve(state.prices);
        });
      }
      return new Promise((resolve) => {
        rf.getRequest('PriceRequest').getPrices().then((res) => {
          commit('updatePrice', res.data);
          resolve(res.data);
        });
      });
    },
    fetchBonuses ({ commit }) {
      return new Promise((resolve) => {
        rf.getRequest('BonusRequest').getAll({
          sortField: 'timestamp',
          sortDirection: 'asc',
        }).then((res) => {
          commit('updateBonuses', res.data);
          resolve(res.data);
        });
      });
    },
  },
});

export default store;
