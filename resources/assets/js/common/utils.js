import _ from 'lodash';
import BigNumber from 'bignumber.js';

export default {
  formatCurrencyAmount(amount, currency, zeroValue, isRoundUp = false) {
    let precision = 8;

    if (window.app.$store && window.app.$store.getters.getPrecision) {
      precision = window.app.$store.getters.getPrecision(currency);
    }

    if (typeof amount === 'string') {
      amount.replace(',', '');
    }

    if (!amount || parseFloat(amount) === 0) {
      return window._.isNil(zeroValue) ? '' : zeroValue;
    }

    const result = BigNumber(amount).toFormat(precision, isRoundUp ? BigNumber.ROUND_UP : BigNumber.ROUND_DOWN);
    if ((typeof result === 'string') && result.includes('.')) {
      return _.trimEnd(_.trimEnd(result, '0'), '.');
    }
    return result;
  },

  setI18nLocale(locale) {
    window.i18n.locale = locale;
    window.app.$broadcast('UPDATED_LOCALE', locale);
  },

};
