import Vue from 'vue';
import { Validator } from 'vee-validate';
import WAValidator from 'wallet-address-validator';
import bignumber from 'bignumber.js';

Vue.mixin({
  mounted: function () {
    if(this.hasValidateFunc) {
      this.$on('UPDATED_LOCALE', () => {
        if(this.errors.count() > 0) {
          _.forEach(this.getValidateFucName(), (fucName) => {
            this[fucName]();
          });
        }
      });
    }
  },
  computed: {
    hasValidateFunc() {
      return _.chain(this).keys().some((key) => {
        return _.startsWith(key, 'validate')
      }).value();
    }
  },
  methods: {
    getValidateFucName() {
      return _.chain(this).keys().filter((key) => {
              return _.startsWith(key, 'validate')
            }).value();
    }
  },
  beforeDestroy() {
    if(this.hasValidateFunc) {
      this.$off('UPDATED_LOCALE');
    }
  }
});

// custom rule
Validator.extend('is_eth_address', {
  getMessage: field => 'The ' + field + ' must be a valid Ethereum address.',
  validate: (value) => {
    return WAValidator.validate(value, 'ETH');
  }
});

Validator.extend('is_btc_address', {
  getMessage: field => 'The ' + field + ' must be a valid Bitcoin address.',
  validate: (value) => {
    return WAValidator.validate(value, 'BTC');
  }
});

Validator.extend('is_usdt_address', {
  getMessage: field => 'The ' + field + ' must be a valid TetherUS address.',
  validate: (value) => {
    return WAValidator.validate(value, 'BTC');
  }
});

Validator.extend('gt', {
  getMessage: (field, params) => 'The ' + field + ' must be greater than ' + params[0],
  validate: (value, params) => {
    return bignumber(value).comparedTo(params[0]) > 0;
  }
});

Validator.extend('no_space', {
  getMessage: field => 'The ' + field + ' must not contain white space',
  validate: value => {
    return !_.includes(value, ' ');
  }
});
