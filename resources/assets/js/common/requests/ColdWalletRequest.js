import BaseRequest from '../BaseRequest'

export default class ColdWalletRequest extends BaseRequest {
  getEndpoint() {
    return '/api/cold-wallets'
  }
}
