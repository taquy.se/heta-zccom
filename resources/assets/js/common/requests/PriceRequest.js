import BaseRequest from '../BaseRequest'

export default class PriceRequest extends BaseRequest {

  getPrices() {
    return new Promise((resolve, reject) => {
      const url = '/api/prices';
      return this._get(url)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
