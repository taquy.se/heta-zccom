import BaseRequest from '../BaseRequest'

export default class UserKycRequest extends BaseRequest {

  getEndpoint() {
    return '/api/userKycs'
  }

  getSumSubToken () {
    const url = `${this.getEndpoint()}/get-sumsub-token`;
    return this._get(url);
  }

  getApplicant () {
    const url = `${this.getEndpoint()}/get-applicant`;
    return this._get(url);
  }
}
