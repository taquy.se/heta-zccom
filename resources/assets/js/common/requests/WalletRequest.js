import BaseRequest from '../BaseRequest'

export default class WalletRequest extends BaseRequest {

  getEndpoint() {
    return '/api/wallet'
  }

  getTransactions (params) {
    const url = `${this.getEndpoint()}/transactions`;
    return this._get(url, params);
  }

  getHetaFluctuations (params) {
    const url = `${this.getEndpoint()}/heta-fluctuations`;
    return this._get(url, params);
  }

  generateDepositAddress (coin) {
    const url = `${this.getEndpoint()}/generate-deposit-address`;
    return this.post(url, { coin });
  }

  getDistributionHetaTokens (params) {
    const url = `${this.getEndpoint()}/distribution-heta-tokens`;
    return this._get(url, params);
  }

  approveBountyWithdraw (params) {
    const url = `${this.getEndpoint()}/approve-bounty-withdraw`;
    return this.put(url, params);
  }
}
