import BaseRequest from '../BaseRequest'

export default class BountySettingRequest extends BaseRequest {

  getEndpoint() {
    return '/api/bountySettings'
  }

  getAdminBountySettings(params) {
    const url  = '/api/admin/bountySettings';
    return this._get(url, params);
  }
}
