import UserRequest from './UserRequest';
import WhitelistRequest from './WhitelistRequest';
import UserKycRequest from './UserKycRequest';
import BountyRequest from './BountyRequest';
import BountySettingRequest from './BountySettingRequest';
import VoucherRequest from './VoucherRequest';
import PriceRequest from './PriceRequest';
import WalletRequest from './WalletRequest';
import SystemSettingRequest from './SystemSettingRequest';
import BonusRequest from './BonusRequest';
import ColdWalletRequest from './ColdWalletRequest';
import StatisticRequest from './StatisticRequest';
import ConversionRequest from './ConversionRequest';
import BugBountyRequest from './BugBountyRequest';

const requestMap = {
  UserRequest,
  WhitelistRequest,
  UserKycRequest,
  BountyRequest,
  BountySettingRequest,
  VoucherRequest,
  PriceRequest,
  WalletRequest,
  SystemSettingRequest,
  BonusRequest,
  ColdWalletRequest,
  StatisticRequest,
  ConversionRequest,
  BugBountyRequest,
};

const instances = {};

export default class RequestFactory {
  static getRequest(classname) {
    const RequestClass = requestMap[classname];
    if (!RequestClass) {
      throw new Error('Invalid request class name: ' + classname);
    }

    let requestInstance = instances[classname];
    if (!requestInstance) {
      requestInstance = new RequestClass();
      instances[classname] = requestInstance;
    }

    return requestInstance;
  }
}
