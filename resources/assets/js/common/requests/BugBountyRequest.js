import BaseRequest from '../BaseRequest'

export default class BugBountyRequest extends BaseRequest {

  getEndpoint() {
    return '/api/bugBounties';
  }

  getAllBugBounties (params) {
    const url = `${this.getEndpoint()}/get-all`;
    return this._get(url, params);
  }

  approveAllType () {
    const url = `${this.getEndpoint()}/approve-all`;
    return this.post(url);
  }
}
