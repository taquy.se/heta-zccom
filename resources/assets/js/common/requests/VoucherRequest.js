import BaseRequest from '../BaseRequest'

export default class VoucherRequest extends BaseRequest {

  getEndpoint() {
    return '/api/vouchers'
  }
}
