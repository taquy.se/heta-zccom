import BaseRequest from '../BaseRequest'

export default class UserRequest extends BaseRequest {

  getEndpoint () {
    return '/api/users'
  }

  getCurrentUser (params = {}) {
    const url = `${this.getEndpoint()}/current-user`;
    return this._get(url, params);
  }

  getUserReport (params) {
    const url = `/api/admin/users/get-user-report`;
    return this._get(url, params);
  }

  resetPassword (params) {
    const url = `${this.getEndpoint()}/reset-password`;
    return this.post(url, params);
  }

  submitResetPassword (params) {
    const url = `${this.getEndpoint()}/submit-reset-password`;
    return this.post(url, params);
  }

  changePassword (params) {
    const url = '/change-password';
    return new Promise((resolve, reject) => {
      window.axios
        .put(url, params)
        .then((response) => {
          resolve(response.request.responseURL);
        })
        .catch((error) => {
          this._errorHandler(reject, error);
        });
    });
  }

  updateUserInfoAfterKyc () {
    const url = `${this.getEndpoint()}/update-user-info-after-kyc`;
    return this.put(url);
  }

  login (params) {
    const url = '/login';
    return new Promise((resolve, reject) => {
      window.axios
        .post(url, params)
        .then((response) => {
          resolve(response.request.responseURL);
        })
        .catch((error) => {
          this._errorHandler(reject, error);
        });
    });
  }

  adminLogin (params) {
    const url = '/admin-login';
    return new Promise((resolve, reject) => {
      window.axios
        .post(url, params)
        .then((response) => {
          resolve(response.request.responseURL);
        })
        .catch((error) => {
          this._errorHandler(reject, error);
        });
    });
  }

  getReferralProgramRecords (params) {
    const url = `${this.getEndpoint()}/referral-program-records`;
    return this._get(url, params);
  }

  getEstHetaTokens (params) {
    const url = `${this.getEndpoint()}/est-heta-tokens`;
    return this._get(url, params);
  }

  getTotalHetaTokens(params) {
    const url = `${this.getEndpoint()}/total-heta-tokens`;
    return this._get(url, params);
  }

  getQRCodeGoogleUrl () {
    const url = `${this.getEndpoint()}/qr-code-google-url`;
    return this._get(url);
  }

  addSecuritySettingOtp (params) {
    const url = `${this.getEndpoint()}/add-security-setting-otp`;
    return this.post(url, params);
  }

  removeAuthentication (params) {
    const url = `${this.getEndpoint()}/remove-security-setting-otp`;
    return this.post(url, params);
  }

  changeAddress (params) {
    const url = `${this.getEndpoint()}/change-address`;
    return this.post(url, params);
  }

  setLocale(params) {
    let url = '/locale';
    return this.put(url, params);
  }

  sendVerifyUser(params) {
    let url = '/api/send-verify-user';
    return this.post(url, params);
  }
}
