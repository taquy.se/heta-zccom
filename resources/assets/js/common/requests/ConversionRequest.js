import BaseRequest from '../BaseRequest'

export default class ConversionRequest extends BaseRequest {
  getEndpoint() {
    return '/api/conversions'
  }

  getSettings() {
    const url = `${this.getEndpoint()}/settings`;
    return this._get(url);
  }

  getSold () {
    const url = `${this.getEndpoint()}/sold`;
    return this._get(url);
  }
}
