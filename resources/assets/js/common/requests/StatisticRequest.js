import BaseRequest from '../BaseRequest'

export default class StatisticRequest extends BaseRequest {

  getConversionStatistic(params) {
    const url = '/api/conversion-statistic';
    return this._get(url, params)
  }

  getUserStatistic(params) {
    const url = '/api/kyc-statistic';
    return this._get(url, params)
  }

  getSpentVsDepositedStatistic(params) {
    const url = '/api/stats/spent-vs-deposited';
    return this._get(url, params)
  }
}
