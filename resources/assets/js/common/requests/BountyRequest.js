import BaseRequest from '../BaseRequest'

export default class BountyRequest extends BaseRequest {

  getEndpoint() {
    return '/api/bounties'
  }

  getSocialBounty () {
    const url = `${this.getEndpoint()}/social`;
    return this._get(url);
  }

  getAllPosts () {
    const url = `${this.getEndpoint()}/posts`;
    return this._get(url);
  }

  getBountyRegister () {
    const url = `${this.getEndpoint()}/register`;
    return this._get(url);
  }

  getAllBountyVoucher () {
    const url = `${this.getEndpoint()}/vouchers`;
    return this._get(url);
  }

  createBountyVoucher (params) {
    const url = `${this.getEndpoint()}/vouchers`;
    return this.post(url, params);
  }

  approveAllType (params) {
    const url = `${this.getEndpoint()}/approve-all-type`;
    return this.post(url, params);
  }
}
