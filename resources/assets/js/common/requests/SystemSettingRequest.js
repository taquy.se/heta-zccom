import BaseRequest from '../BaseRequest'

export default class SystemSettingRequest extends BaseRequest {
  getEndpoint() {
    return '/api/system-setting'
  }

  updateSetting(params) {
    const url = '/api/system-setting';
    return this.put(url, params);
  }
}
