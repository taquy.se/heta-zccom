import BaseRequest from '../BaseRequest'

export default class WhitelistRequest extends BaseRequest {

  getEndpoint() {
    return '/api/whitelists'
  }
}
