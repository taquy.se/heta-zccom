import BaseRequest from '../BaseRequest'

export default class BonusRequest extends BaseRequest {

  getEndpoint() {
    return '/api/bonuses'
  }

  getAdminBonuses(params) {
    const url = '/api/admin/bonuses';
    return this._get(url, params);
  }
}
