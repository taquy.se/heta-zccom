import Vue from 'vue';
import Utils from './utils';
import _ from 'lodash';

Vue.filter('expectedConfirmation', function(currency) {
  if (window.app.$store && window.app.$store.getters.expectedConfirmation) {
    return window.app.$store.getters.expectedConfirmation(currency);
  }
  return 0;
});

Vue.filter('minDeposit', function(currency) {
  if (window.app.$store && window.app.$store.getters.minDeposit) {
    return window.app.$store.getters.minDeposit(currency);
  }
  return 0;
});

Vue.filter('transactionStatus', function({status, currency, confirmations}) {
  const expectedConfirmation = window.app.$store.getters.expectedConfirmation(currency);
  return status === 'confirming' ? `${confirmations}/${expectedConfirmation}` : status;
});

Vue.filter('formatCurrencyAmount', function(amount, currency, zeroValue, isRoundUp = false) {
  return Utils.formatCurrencyAmount(amount, currency, zeroValue, isRoundUp);
});

Vue.filter('uppercase', function (value) {
  return _.toUpper(value);
});

Vue.filter('upperFirst', function (value) {
  return _.upperFirst(value);
});

Vue.filter('txExplore', function (transaction) {
  const explorers = {
    eth: global.ETH_EXPLORER_URL,
    btc: global.BTC_EXPLORER_URL,
    usdt: global.USDT_EXPLORER_URL,
  };
  const explorer = explorers[transaction.currency] || explorers.btc;
  return `${explorer}/${transaction.txid}`;
})

Vue.filter('addressExplore', function (wallet) {
  const explorers = {
    eth: 'https://etherscan.io/address',
    btc: 'https://www.blockchain.com/btc/address',
    usdt: 'https://omniexplorer.info/address',
  };
  const explorer = explorers[wallet.currency] || explorers.btc;
  return `${explorer}/${wallet.address}`;
})

Vue.filter('currencyIcon', function (symbol) {
  return `/images/icon/balanceImg/heta-${symbol}-icon.png`;
})

Vue.filter('fullname', function (symbol) {
  const fullnames = { eth: 'Ethereum', btc: 'Bitcoin', usdt: 'TetherUS' };
  return fullnames[symbol];
})
