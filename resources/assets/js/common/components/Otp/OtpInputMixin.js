export default {
  data () {
    return {
      loading: false,
    };
  },
  methods: {
    resetError () {
      this.errors.clear();
    },
    isNumber ($event) {
      this.resetError();
      const keyCode = ($event.keyCode ? $event.keyCode : $event.which);
      if ((keyCode < 48 || keyCode > 57)) {
          $event.preventDefault();
      }
    },
    handleEventKeyUp ($event) {
      const keyCode = ($event.keyCode ? $event.keyCode : $event.which);
      if ((keyCode < 48 || keyCode > 57)) {
          return;
      }
      if (this.code.length === 6) {
        this.verifyCode();
      }
    },
  }
};