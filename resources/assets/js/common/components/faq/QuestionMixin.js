export default {
  props: {
    question: {
      type: Object,
    }
  },
  data () {
    return {
      isCollapse: false,
    };
  },
  methods: {
    turnOffCollapse () {
      this.isCollapse = false;
    },
    toggle () {
      this.isCollapse = !this.isCollapse;
    },
  },
};
