export default {
  data() {
    return {
      questions: [
        {
          id: 1,
          questionContent: 'faq.questions.text1',
          answer: 'faq.answer.text1',
        },
        {
          id: 2,
          questionContent: 'faq.questions.text2',
          answer: 'faq.answer.text2',
        },
        {
          id: 3,
          questionContent: 'faq.questions.text3',
          answer: 'faq.answer.text3',
        },
        {
          id: 4,
          questionContent: 'faq.questions.text4',
          answer: 'faq.answer.text4',
        },
        {
          id: 5,
          questionContent: 'faq.questions.text5',
          answer: 'faq.answer.text5',
        },
        {
          id: 6,
          questionContent: 'faq.questions.text6',
          answer: 'faq.answer.text6',
        },
        {
          id: 7,
          questionContent: 'faq.questions.text7',
          answer: 'faq.answer.text7',
        },
        {
          id: 8,
          questionContent: 'faq.questions.text8',
          answer: 'faq.answer.text8',
        },
        {
          id: 9,
          questionContent: 'faq.questions.text9',
          answer: 'faq.answer.text9',
        },
        {
          id: 10,
          questionContent: 'faq.questions.text10',
          answer: 'faq.answer.text10',
        },
        {
          id: 11,
          questionContent: 'faq.questions.text11',
          answer: 'faq.answer.text11',
        },
        {
          id: 12,
          questionContent: 'faq.questions.text12',
          answer: 'faq.answer.text12',
        },
        {
          id: 13,
          questionContent: 'faq.questions.text13',
          answer: 'faq.answer.text13',
        },
        {
          id: 14,
          questionContent: 'faq.questions.text14',
          answer: 'faq.answer.text14',
        },
        {
          id: 15,
          questionContent: 'faq.questions.text15',
          answer: 'faq.answer.text15',
        },
      ]
    };
  },
};
