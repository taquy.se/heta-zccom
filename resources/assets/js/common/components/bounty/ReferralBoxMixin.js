import { mapState } from 'vuex';

export default {
  computed: {
    ...mapState([
      'user',
    ]),
    urlReferral () {
      if (!this.user || !this.user.referral_id) {
        return '';
      }
      return this.user ? `${this.$baseUrl}/register?referral_id=${this.user.referral_id}` : '';
    }
  },
};
