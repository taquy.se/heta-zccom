import Modal from './Modal.vue';

export default {
  components: {
    Modal,
  },
  data () {
    return {
      displayModal: false,
      modalData: null,
      currentModal: null,
    };
  },
  created () {
    this.$on('open-modal', this.openModal);
    this.$on('close-modal', this.modalOff);
  },
  methods: {
    modalOff() {
      this.displayModal = false;
      this.currentModal = null;
    },
    openModal(component, modalData = null) {
      this.currentModal = component;
      this.modalData = modalData;
      this.displayModal = true;
    },
  },
}
