export default {
  created () {
    if (this.$isAuthenticated) {
      this.$router.push('/');
    }
  }
}
