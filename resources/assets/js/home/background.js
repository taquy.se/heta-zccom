import Vue from "vue";
import store from "../common/store";
import Background from './components/Dashboard/Background';
import GuestPageFooter from './components/GuestPageFooter.vue';

window._ = require('lodash');
window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
  window.$ = window.jQuery = require('jquery');

  require('bootstrap');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
  window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
  console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

import VueI18n from 'vue-i18n';
import Messages from "../common/lang/vue-i18n";
Vue.use(VueI18n);
import '../common/filters';
import ClickOutside from 'vue-click-outside';
Vue.directive('click-outside', ClickOutside);
import VueBroadcast from '../common/VueBroadcast';
Vue.use(VueBroadcast);

const locale = document.documentElement.lang || 'en';
window.i18n = new VueI18n({
  locale: locale,
  messages: Messages
});

const i18n = window.i18n;

new Vue({
  i18n,
  store,
  el: '#app',
  components: {
    Background,
    GuestPageFooter
  }
});
