import Vue from 'vue';
import ClickOutside from 'vue-click-outside';
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';
import Toasted from 'vue-toasted';
import VTooltip from 'v-tooltip';
import VueDatepicker from 'vuejs-datepicker';
import SocialSharing from 'vue-social-sharing';

import VueClipboard from 'vue-clipboard2';
import VueQrcode from '@xkeshi/vue-qrcode';
import pagination from "vuejs-uib-pagination";
import GlobalSocket from '../common/GlobalSocket';

import '../common/bootstrap';
import VueBroadcast from '../common/VueBroadcast';
import routes from './routes';
import store from '../common/store';
import App from './App.vue'

Vue.directive('click-outside', ClickOutside);
Vue.use(VueBroadcast);
Vue.use(VueRouter);
Vue.use(Toasted);
Vue.use(VTooltip);
Vue.use(VueClipboard);
Vue.use(pagination);
Vue.use(SocialSharing);

Vue.prototype.$csrfToken = document.head.querySelector('meta[name="csrf-token"]').getAttribute('content');
Vue.prototype.$baseUrl = document.head.querySelector('meta[name="base-url"]').getAttribute('content');
Vue.prototype.$isAuthenticated = !!document.head.querySelector('meta[name="authenticated"]');
Vue.component('vue-datepicker', VueDatepicker);
Vue.component(VueQrcode.name, VueQrcode);

const router = new VueRouter({
  routes,
  mode: 'history',
  linkExactActiveClass: 'nav-item active'
});

const i18n = window.i18n;

Vue.use(VeeValidate);
import '../common/validators';
import '../common/filters';

import DataTable from './components/DataTable.vue';
Vue.component('data-table', DataTable);

import Background from './components/Dashboard/Background';
Vue.component('background', Background);

router.beforeEach((to, from, next) => {
  document.title = 'HetaChain Platform' + (to.name ? ' - ' +  to.name: '');
  next();
});

window.app = new Vue({
  i18n,
  store,
  el: '#app',
  render: h => h(App),
  router,
  created () {
    if (this.$isAuthenticated) {
      this.$store.dispatch('fetchPrices');
      this.$store.dispatch('fetchSystemSettings');
      this.$store.dispatch('fetchConversionSettings');
    }
  }
});

window.GlobalSocket = new GlobalSocket();
