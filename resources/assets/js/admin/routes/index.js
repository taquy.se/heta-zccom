import Layout from '../pages/Layout/DashboardLayout.vue';
import Login from '../pages/Auth/Login.vue';
import User from '../pages/User.vue';
import Whitelist from '../pages/Whitelist.vue';
import UserReport from '../pages/UserReport.vue';
import Bounty from '../pages/Bounty.vue';
import BountySetting from '../pages/BountySetting.vue';
import Voucher from '../pages/Voucher.vue';
import Setting from '../pages/Setting.vue';
import ColdWallets from '../pages/ColdWallets.vue';
import Statistic from '../pages/Statistic.vue';
import Distribution from '../pages/Distribution.vue';
import BugBounty from '../pages/BugBounty.vue';

const routes = [
  {
    path: '/admin/login',
    component: Login,
    name: 'Login'
  },
  {
    path: '/admin',
    component: Layout,
    redirect: '/admin/users',
    children: [
      {
        path: '/admin/users/report',
        name: 'User Report',
        component: UserReport
      },
      {
        path: '/admin/users',
        name: 'User',
        component: User
      },
      {
        path: '/admin/whitelists',
        name: 'Whitelist',
        component: Whitelist
      },
      {
        path: '/admin/bounties',
        name: 'Bounties',
        component: Bounty
      },
      {
        path: '/admin/bug-bounties',
        name: 'Bug Bounties',
        component: BugBounty
      },
      {
        path: '/admin/bounty-settings',
        name: 'Bounty Settings',
        component: BountySetting
      },
      {
        path: '/admin/vouchers',
        name: 'Voucher',
        component: Voucher
      },
      {
        path: '/admin/settings',
        name: 'Setting',
        component: Setting
      },
      {
        path: '/admin/cold-wallets',
        name: 'Cold Wallets',
        component: ColdWallets
      },
      {
        path: '/admin/statistic',
        name: 'Statistic',
        component: Statistic
      },
      {
        path: '/admin/distribution',
        name: 'Distribution Heta Token',
        component: Distribution
      },
    ]
  },
  { path: '*', redirect: '/admin/users' }
]

export default routes;
