<template>
  <div>
    <data-table ref="datatable"
                :get-data="getBountySettings"
                :limit="100">
      <template slot="table-toolbar">
        <div class="md-toolbar-section-start">
          <h1 class="md-title">Bounty & Airdrop Settings</h1>
        </div>

        <md-field md-clearable class="md-field-search md-toolbar-section-end">
          <md-input placeholder="Search by name ..." v-model="search" @input="searchSettings"/>
        </md-field>
      </template>

      <md-table-head>#</md-table-head>
      <md-table-head>Name</md-table-head>
      <md-table-head>Tokens</md-table-head>
      <md-table-head></md-table-head>

      <template slot="body" slot-scope="{ item, index }">
        <md-table-row >
          <md-table-cell> {{ index + 1 }}</md-table-cell>
          <md-table-cell>{{ item.display_name }}</md-table-cell>
          <md-table-cell>
            <md-field :class="{'md-invalid': errors.has(item.key)}">
              <md-input
                :key="item.key"
                :name="item.key"
                :data-vv-as="item.display_name"
                v-model="item.value"
                v-validate="'required|numeric|min_value:0'"
                data-vv-validate-on="none"
                type="number"
                placeholder="tokens"></md-input>
              <span class="md-error" v-if="errors.has(item.key)">{{ errors.first(item.key) }}</span>
            </md-field>
          </md-table-cell>
          <md-table-cell>
            <md-button class="md-raised md-button-primary" @click="showConfirmDialog(item)">Save</md-button>
          </md-table-cell>
        </md-table-row>
      </template>
    </data-table>
    <md-dialog-confirm
      :md-active.sync="isShowConfirmDialog"
      md-title="Are you sure you want to update?"
      md-confirm-text="Agree"
      md-cancel-text="Disagree"
      @md-cancel="onCancelUpdate"
      @md-confirm="onConfirmUpdate" />
  </div>
</template>

<script>
  import rf from '../../common/requests/RequestFactory';

  export default {
    data () {
      return {
        search: '',
        isShowConfirmDialog: false,
        settingWillUpdate: {},
      }
    },
    methods: {
      showConfirmDialog(setting) {
        this.$validator.validate(setting.key)
          .then((result) => {
            if (!result) {
              return;
            }
            this.isShowConfirmDialog = true;
            this.settingWillUpdate = setting;
          });
      },
      onConfirmUpdate () {
        this.updateSetting(this.settingWillUpdate);
        this.settingWillUpdate = {};
        this.$toasted.show('You updated successfully!', {
          theme: 'bubble',
          position: 'top-right',
          duration : 2000
        });
      },
      onCancelUpdate () {
        this.settingWillUpdate = {};
        this.refreshDataTable();
      },

      refreshDataTable () {
        this.$refs.datatable.refresh();
      },

      searchSettings: _.debounce(function() {
        this.refreshDataTable();
      }, 500),

      getBountySettings (params) {
        const meta = Object.assign({}, {
          like_operator: 'or',
          like: {
            display_name: this.search,
          },
          sortField: 'display_order',
          sortDirection: 'asc'
        });
        return rf.getRequest('BountySettingRequest').getAdminBountySettings(Object.assign({}, params, meta));
      },
      updateSetting (setting) {
        this.$validator.validate(setting.key)
          .then((result) => {
            if (!result) {
              return;
            }
            rf.getRequest('BountySettingRequest').update(setting.id, setting).then(res => {
              const settingIndex = _.findIndex(this.$refs.datatable.rows, { id: res.data.id });
              this.$refs.datatable.rows.splice(settingIndex, 1, res.data);
            });
          });
      }
    },
  }
</script>

<style lang="scss" scoped>
  .md-field-search {
    max-width: 400px;
  }

  .md-card .md-card-content {
    padding: 0px 20px;
  }

  .md-card {
    margin: 0px;
  }
</style>
