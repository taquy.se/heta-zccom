import Vue from 'vue';
import ClickOutside from 'vue-click-outside';
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';
import Toasted from 'vue-toasted';
import pagination from "vuejs-uib-pagination";
import VueDatepicker from 'vuejs-datepicker';
import VueHighcharts from 'vue-highcharts';

import '../common/bootstrap';
import VueBroadcast from '../common/VueBroadcast';
import routes from './routes';
import App from './App.vue'
import MaterialDashboard from './material-dashboard';
import DataTable from './components/DataTable.vue';
import store from './store';

Vue.directive('click-outside', ClickOutside);
Vue.use(VueBroadcast);
Vue.use(VueRouter);
Vue.use(Toasted);
Vue.use(MaterialDashboard);
Vue.use(pagination);
Vue.use(VueHighcharts);
import 'vue-datetime/dist/vue-datetime.css'
import { Datetime } from 'vue-datetime';
Vue.component('datetime', Datetime);

const i18n = window.i18n;

Vue.use(VeeValidate);
import '../common/validators';

import '../common/filters';

Vue.prototype.$csrfToken = document.head.querySelector('meta[name="csrf-token"]').getAttribute('content');
Vue.prototype.$baseUrl = document.head.querySelector('meta[name="base-url"]').getAttribute('content');
Vue.prototype.$isAuthenticated = !!document.head.querySelector('meta[name="authenticated"]');
Vue.component('datepicker', VueDatepicker);
Vue.component('data-table', DataTable);

const router = new VueRouter({
  routes,
  mode: 'history',
  linkExactActiveClass: 'nav-item active'
});

window.app = new Vue({
  store,
  i18n,
  el: '#app',
  render: h => h(App),
  router,
});
