// Tables
import UsersTable from './UsersTable.vue'
import WhitelistTable from './WhitelistTable.vue'

export {
  UsersTable,
  WhitelistTable,
}
