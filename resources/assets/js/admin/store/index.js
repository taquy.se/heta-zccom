import Vue from 'vue';
import Vuex from 'vuex';
import rf from '../../common/requests/RequestFactory';;

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    user: {},
    key2FA: '',
    coldWallets: [],
    systemSettings: {},
    conversionSettings: [],
  },
  mutations: {
    updateUser (state, data) {
      state.user = data;
    },
    update2FaSecret (state, data) {
      state.key2FA = data;
    },
    updateColdWallets (state, data) {
      state.coldWallets = data;
    },
    updateSystemSettings (state, data) {
      state.systemSettings = data;
    },
    updateConversionSettings (state, data) {
      state.conversionSettings = data;
    },
    getPrecision: ({ conversionSettings }) => (currency) => {
      currency = _.toLower(currency);
      if (currency === 'heta') {
        const conversionSetting = _.head(conversionSettings) || {};
        return conversionSetting.token_precision || 0;
      }

      if (currency === 'usd') {
        currency = 'usdt';
      }

      const conversionSetting = conversionSettings.find((item) => item.currency === currency) || {};
      return conversionSetting.currency_precision || 8;
    }
  },
  actions: {
    getCurrentUser ({ commit }) {
      return new Promise((resolve) => {
        rf.getRequest('UserRequest').getCurrentUser().then((res) => {
          commit('updateUser', res.data);
          resolve(res);
        });
      });
    },
    fetchColdWallets ({ commit }) {
      return new Promise((resolve) => {
        rf.getRequest('ColdWalletRequest').getAll().then((res) => {
          commit('updateColdWallets', res.data);
          resolve(res);
        });
      });
    },
    fetchConversionSettings ({ commit }) {
      return new Promise((resolve) => {
        rf.getRequest('ConversionRequest').getSettings().then((res) => {
          commit('updateConversionSettings', res.data);
          resolve(res.data);
        });
      });
    },
    fetchSystemSettings ({ commit }) {
      return new Promise((resolve) => {
        rf.getRequest('SystemSettingRequest').getAll().then((res) => {
          commit('updateSystemSettings', res.data);
          resolve(res.data);
        });
      });
    },
  },
});

export default store;
